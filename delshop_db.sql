-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 12, 2018 at 07:54 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `delshop_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_kategorijasa`
--

CREATE TABLE `detail_kategorijasa` (
  `id_detail` int(11) NOT NULL,
  `keterangan_detail` varchar(255) DEFAULT NULL,
  `nama_detail` varchar(255) DEFAULT NULL,
  `id_kategorijasa` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_kategorijasa`
--

INSERT INTO `detail_kategorijasa` (`id_detail`, `keterangan_detail`, `nama_detail`, `id_kategorijasa`) VALUES
(1, NULL, 'Desain Baju', NULL),
(2, NULL, 'Desain Logo', NULL),
(3, '', 'Editing Vidio', NULL),
(4, NULL, 'Editing Gambar', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jasa`
--

CREATE TABLE `jasa` (
  `id_jasa` int(11) NOT NULL,
  `deskripsi_jasa` varchar(255) DEFAULT NULL,
  `nama_jasa` varchar(255) DEFAULT NULL,
  `id_kategorijasa` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jasa`
--

INSERT INTO `jasa` (`id_jasa`, `deskripsi_jasa`, `nama_jasa`, `id_kategorijasa`, `id`) VALUES
(1, 'dwwsd', 'Desain Logo', 2, 1),
(3, 'das', 'Desain Logo', 2, 1),
(5, 'ad', 'Desain Logo', 2, 1),
(6, 'perjam ', 'Tutor Madas', 4, 1),
(7, '@jam', 'Tutor Fisdas', 4, 1),
(8, '@Video', 'edit Vidio', 2, 1),
(9, '', '', 1, 1),
(10, 'an hour i will make good logo', 'clarita', 1, 7);

-- --------------------------------------------------------

--
-- Table structure for table `kategori_jasa`
--

CREATE TABLE `kategori_jasa` (
  `id_kategorijasa` int(11) NOT NULL,
  `nama_kategorijasa` varchar(255) DEFAULT NULL,
  `id_kategori_jasa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_jasa`
--

INSERT INTO `kategori_jasa` (`id_kategorijasa`, `nama_kategorijasa`, `id_kategori_jasa`) VALUES
(1, 'Desain', 0),
(2, 'Editing', 0),
(3, 'Les Musik', 0),
(4, 'Tutorial', 0);

-- --------------------------------------------------------

--
-- Table structure for table `kategori_produk`
--

CREATE TABLE `kategori_produk` (
  `id_kategoriproduk` int(11) NOT NULL,
  `nama_kategoriproduk` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_produk`
--

INSERT INTO `kategori_produk` (`id_kategoriproduk`, `nama_kategoriproduk`) VALUES
(1, 'Buku'),
(2, 'Kesehatan'),
(3, 'Komputer & Aksesoris'),
(4, 'Laptop & Aksesoris'),
(5, 'Makanan & Minuman'),
(6, 'Olahraga'),
(7, 'Pakaian'),
(8, 'Sayur'),
(9, 'Sovenir & Kado'),
(10, 'Perangkat Lunak');

-- --------------------------------------------------------

--
-- Table structure for table `memesan_jasa`
--

CREATE TABLE `memesan_jasa` (
  `id_pemesananjasa` int(11) NOT NULL,
  `catatan` varchar(255) DEFAULT NULL,
  `status_pemesananjasa` varchar(255) DEFAULT NULL,
  `tanggal_terimajasa` varchar(255) DEFAULT NULL,
  `id_jasa` int(11) DEFAULT NULL,
  `id` int(11) DEFAULT NULL,
  `id_pemesan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `memesan_jasa`
--

INSERT INTO `memesan_jasa` (`id_pemesananjasa`, `catatan`, `status_pemesananjasa`, `tanggal_terimajasa`, `id_jasa`, `id`, `id_pemesan`) VALUES
(1, 'ga ada ', 'Menunggu Pembayaran', '2018-06-12 23:09:21', 3, 1, NULL),
(2, 'tidak', 'Dibayar', '2018-06-12 23:14:20', 5, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `memesan_produk`
--

CREATE TABLE `memesan_produk` (
  `id_pemesananproduk` int(11) NOT NULL,
  `ciri_produk` varchar(255) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `id_produk` int(11) DEFAULT NULL,
  `id` int(11) DEFAULT NULL,
  `total_harga` decimal(19,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `memesan_produk`
--

INSERT INTO `memesan_produk` (`id_pemesananproduk`, `ciri_produk`, `jumlah`, `status`, `id_produk`, `id`, `total_harga`) VALUES
(1, 'tidak', 1, 'Menunggu Pembayaran', 15, 1, NULL),
(2, 'Kosong', 6, 'Menunggu Pembayaran', 15, 1, '600000.00'),
(3, '', 4, 'Dibayar', 16, 1, '6000.00'),
(4, 'ga', 4, 'Menunggu Pembayaran', 15, 1, '400000.00'),
(5, 'pedas', 2, 'Dibayar', 18, 1, '10000.00');

-- --------------------------------------------------------

--
-- Stand-in structure for view `pembayaran`
-- (See below for the actual view)
--
CREATE TABLE `pembayaran` (
`id_pemesananproduk` int(11)
,`total_harga` decimal(19,2)
,`deskripsi` varchar(255)
,`foto_produk` varchar(255)
,`nama_produk` varchar(255)
,`jumlah` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_jasa`
--

CREATE TABLE `pembayaran_jasa` (
  `id_pembayaranjasa` int(11) NOT NULL,
  `metode_pembayaran` varchar(255) DEFAULT NULL,
  `rincian_pembayaranjasa` varchar(255) DEFAULT NULL,
  `status_pembayaranjasa` varchar(255) DEFAULT NULL,
  `tanggal_pembayaran` varchar(255) DEFAULT NULL,
  `id_jasa` int(11) DEFAULT NULL,
  `id_pemesananjasa` int(11) DEFAULT NULL,
  `id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembayaran_jasa`
--

INSERT INTO `pembayaran_jasa` (`id_pembayaranjasa`, `metode_pembayaran`, `rincian_pembayaranjasa`, `status_pembayaranjasa`, `tanggal_pembayaran`, `id_jasa`, `id_pemesananjasa`, `id`) VALUES
(1, 'Transfer', 'gd 911 aja nnti malam ya bang,', 'Menunggu Konfirmasi Pembayaran', '2018-06-13 00:19:35', 5, 2, 1),
(2, 'Bayar Langsung', ',085261579157, jam 7 malam nti ya bang', 'Menunggu Konfirmasi Pembayaran', '2018-06-13 00:34:15', 5, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_produk`
--

CREATE TABLE `pembayaran_produk` (
  `id_pembayaranproduk` int(11) NOT NULL,
  `detail_pembayaranproduk` varchar(255) DEFAULT NULL,
  `metode_pembayaran` varchar(255) DEFAULT NULL,
  `status_pembayaran` varchar(255) DEFAULT NULL,
  `tanggal_pembayaran` varchar(255) DEFAULT NULL,
  `id_pemesananproduk` int(11) DEFAULT NULL,
  `id_produk` int(11) DEFAULT NULL,
  `id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembayaran_produk`
--

INSERT INTO `pembayaran_produk` (`id_pembayaranproduk`, `detail_pembayaranproduk`, `metode_pembayaran`, `status_pembayaran`, `tanggal_pembayaran`, `id_pemesananproduk`, `id_produk`, `id`) VALUES
(1, ',gd 9 ', 'Transfer', 'Selesai', '2018-06-12 01:34:07', NULL, NULL, 1),
(2, 'Gd 9 ,', 'Transfer', 'Selesai', '2018-06-12 01:44:29', NULL, 16, 1),
(3, 'hkhk,', 'Transfer', 'Menunggu Konfirmasi Pembayaran', '2018-06-12 01:49:32', NULL, 18, 1),
(4, 'ahaha,', 'Transfer', 'Selesai', '2018-06-12 01:54:28', NULL, 18, 1),
(5, 'medan ya,', 'Transfer', 'Selesai', '2018-06-12 02:02:06', 5, 18, 1),
(6, 'ke gd 9 ya bang,', 'Transfer', 'Menunggu Konfirmasi Pembayaran', '2018-06-12 10:59:23', 3, 16, 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `pemesananmenunggu`
-- (See below for the actual view)
--
CREATE TABLE `pemesananmenunggu` (
`id_pemesanan` int(11)
,`id_produk` int(11)
,`deskripsi` varchar(255)
,`status` varchar(255)
,`idpenjual` int(11)
,`harga_produk` decimal(19,2)
,`total_harga` decimal(19,2)
,`jumlah` int(11)
,`idpembeli` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `penjualan`
-- (See below for the actual view)
--
CREATE TABLE `penjualan` (
`id_produk` int(11)
,`nama_produk` varchar(255)
,`foto_produk` varchar(255)
,`harga_produk` decimal(19,2)
,`stok` int(11)
,`tanggal_penjualanproduk` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `penjualanjasa`
-- (See below for the actual view)
--
CREATE TABLE `penjualanjasa` (
`id_jasa` int(11)
,`id` int(11)
,`nama_jasa` varchar(255)
,`id_kategorijasa` int(11)
,`harga_jasa` decimal(19,2)
,`status_jasa` varchar(255)
);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_jasa`
--

CREATE TABLE `penjualan_jasa` (
  `id_penjualanjasa` int(11) NOT NULL,
  `harga_jasa` decimal(19,2) DEFAULT NULL,
  `status_jasa` varchar(255) DEFAULT NULL,
  `id_jasa` int(11) DEFAULT NULL,
  `id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan_jasa`
--

INSERT INTO `penjualan_jasa` (`id_penjualanjasa`, `harga_jasa`, `status_jasa`, `id_jasa`, `id`) VALUES
(1, '10000.00', 'Ready', 1, 1),
(2, '100.00', 'Ready', 3, 1),
(3, '100.00', 'Ready', 5, 1),
(4, '10000.00', 'Ready', 6, 1),
(5, '20000.00', 'Ready', 7, 1),
(6, '40000.00', 'Ready', 8, 1),
(7, NULL, 'Ready', 9, 1),
(8, '500000.00', 'Ready', 10, 7);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_produk`
--

CREATE TABLE `penjualan_produk` (
  `id_penjualanproduk` int(11) NOT NULL,
  `harga_produk` decimal(19,2) DEFAULT NULL,
  `status_produk` varchar(255) DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `tanggal_penjualanproduk` varchar(255) DEFAULT NULL,
  `id_produk` int(11) DEFAULT NULL,
  `id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan_produk`
--

INSERT INTO `penjualan_produk` (`id_penjualanproduk`, `harga_produk`, `status_produk`, `stok`, `tanggal_penjualanproduk`, `id_produk`, `id`) VALUES
(10, '1241.00', 'Ready', 10, '2018-06-05 02:21:36', 9, 1),
(16, '100000.00', 'Ready', 7, '2018-06-05 21:10:36', 15, 1),
(17, '1500.00', 'Ready', 100, '2018-06-06 08:24:02', 16, 3),
(18, '40000.00', 'Ready', 4, '2018-06-06 16:41:54', 17, 1),
(19, '5000.00', 'Ready', 400, '2018-06-07 04:54:46', 18, 1),
(20, '2000.00', 'Ready', 10, '2018-06-07 09:12:16', 19, 1),
(21, '100000.00', 'Ready', 20, '2018-06-07 09:12:16', 20, 1);

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id_produk` int(11) NOT NULL,
  `ciri_produk` varchar(255) DEFAULT NULL,
  `deskripsi_produk` varchar(255) DEFAULT NULL,
  `foto_produk` varchar(255) DEFAULT NULL,
  `nama_produk` varchar(255) DEFAULT NULL,
  `id_kategoriproduk` int(11) DEFAULT NULL,
  `id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `ciri_produk`, `deskripsi_produk`, `foto_produk`, `nama_produk`, `id_kategoriproduk`, `id`) VALUES
(9, 'qq', 'qa', 'win2.jpg', '13121', 1, NULL),
(11, 'makanan pedas', '@porsi', 'saksang.jpg', 'saksang', 5, 1),
(15, 'Pedes', 'Enak', 'saksang.jpg', 'Babi Panggang', 5, 1),
(16, 'Pedas ', 'Gurih', 'Kripik SI.jpg', 'Kripik SI', 5, 3),
(17, 'ssss', '12', 'winner.jpg', 'naniura', 5, 1),
(18, 'renyah', 'enak', 'Kripik SI.jpg', 'Kerupuk', 5, 1),
(19, 'baru', 'biru', 'sepatu.jpg', 'baju', 7, 1),
(20, 'enak', 'sapi', 'saksang.jpg', 'bakso', 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tinjauan_jasa`
--

CREATE TABLE `tinjauan_jasa` (
  `id_tinjauanjasa` int(11) NOT NULL,
  `komentar_jasa` varchar(255) DEFAULT NULL,
  `tanggal_tinjauan` varchar(255) DEFAULT NULL,
  `id_jasa` int(11) DEFAULT NULL,
  `id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tinjauan_jasa`
--

INSERT INTO `tinjauan_jasa` (`id_tinjauanjasa`, `komentar_jasa`, `tanggal_tinjauan`, `id_jasa`, `id`) VALUES
(1, 'berapa 1 Logo ?', '2018-06-07 00:22:00', 3, 1),
(2, 'Bisa Nego ? ', '2018-06-07 00:24:21', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tinjauan_produk`
--

CREATE TABLE `tinjauan_produk` (
  `id_tinjauanproduk` int(11) NOT NULL,
  `komentar_produk` varchar(255) DEFAULT NULL,
  `tanggal_tinjauanproduk` varchar(255) DEFAULT NULL,
  `id_produk` int(11) DEFAULT NULL,
  `id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tinjauan_produk`
--

INSERT INTO `tinjauan_produk` (`id_tinjauanproduk`, `komentar_produk`, `tanggal_tinjauanproduk`, `id_produk`, `id`) VALUES
(1, NULL, '2018-06-05 23:04:22', 9, 1),
(2, NULL, '2018-06-05 23:04:22', 9, 2),
(3, 'JIDJDD', '2018-06-05 23:09:24', 9, 1),
(4, 'haha', '2018-06-06 09:23:47', 16, 1),
(5, 'halo', '2018-06-06 15:34:24', 15, 1),
(6, NULL, '2018-06-07 00:12:54', NULL, 1),
(7, 'bakso nya enak', '2018-06-07 09:12:16', 20, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `bio` varchar(255) DEFAULT NULL,
  `confirmation_token` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `foto_profil` varchar(255) DEFAULT NULL,
  `nama_belakang` varchar(255) NOT NULL,
  `nama_depan` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `notelp` varchar(255) DEFAULT NULL,
  `no_telp` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `bio`, `confirmation_token`, `email`, `enabled`, `foto_profil`, `nama_belakang`, `nama_depan`, `password`, `notelp`, `no_telp`) VALUES
(1, NULL, '16963712-afae-49eb-b56a-b6904ceb77f5', 'win@mail.com', b'1111111111111111111111111111111', 'sepatu.jpg', 'khgajaa', 'Winner', '$2a$10$xKie7SueP1teJl1iOSXL9.82dn2d4.pXVvjvolNSbK/5zyhxSUXm6', NULL, NULL),
(2, NULL, '23985b86-b795-4752-906e-2da41961679c', 'vik@mail.com', b'1111111111111111111111111111111', NULL, 'ndre', 'vik', '$2a$10$7KPuiAoilqHnptthP3qHquyqe3SPTCMUZeWcs/yHKwtm1pnIRXRg2', NULL, NULL),
(3, NULL, '11b1d8c6-2f53-4f74-a143-279468978619', 'del@mail.com', b'1111111111111111111111111111111', NULL, 'del', 'delshop', '$2a$10$MQPPong1MFJRzm7Ic2hsluqgSYS5uDYQT3TF6xLxg4nowjKRj9oq6', NULL, '085261579157'),
(4, NULL, '68f2cff1-2b42-475c-af50-ea92a5646fce', 'winner@mail.com', b'1111111111111111111111111111111', NULL, 'immanuel', 'winner', NULL, NULL, '085261579157'),
(5, NULL, '4455a3e0-9ec6-49d0-83bc-d3671b67cb84', 'winer@mail.com', b'1111111111111111111111111111111', NULL, 'immanuel', 'winner', NULL, NULL, '085261579157'),
(6, NULL, '0d74551a-c6cb-4978-afe3-a708a8041b9f', 'jes@mail.com', b'1111111111111111111111111111111', NULL, 'jes', 'je', NULL, NULL, '081264172'),
(7, NULL, 'b3772773-3d08-4793-89f9-5c2c3948e6f9', 'cla@mail.com', b'1111111111111111111111111111111', NULL, 'butar', 'clarita', '$2a$10$FMcJLlkjr.I1yloMX36EZOYVq3.Id/L5/qof70Gx3roumeDrQgszq', NULL, '127567');

-- --------------------------------------------------------

--
-- Structure for view `pembayaran`
--
DROP TABLE IF EXISTS `pembayaran`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `pembayaran`  AS  (select `memesan_produk`.`id_pemesananproduk` AS `id_pemesananproduk`,`memesan_produk`.`total_harga` AS `total_harga`,`memesan_produk`.`ciri_produk` AS `deskripsi`,`produk`.`foto_produk` AS `foto_produk`,`produk`.`nama_produk` AS `nama_produk`,`memesan_produk`.`jumlah` AS `jumlah` from (`memesan_produk` join `produk` on((`memesan_produk`.`id_produk` = `produk`.`id_produk`)))) ;

-- --------------------------------------------------------

--
-- Structure for view `pemesananmenunggu`
--
DROP TABLE IF EXISTS `pemesananmenunggu`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `pemesananmenunggu`  AS  (select `memesan_produk`.`id_pemesananproduk` AS `id_pemesanan`,`penjualan_produk`.`id_produk` AS `id_produk`,`memesan_produk`.`ciri_produk` AS `deskripsi`,`memesan_produk`.`status` AS `status`,`penjualan_produk`.`id` AS `idpenjual`,`penjualan_produk`.`harga_produk` AS `harga_produk`,`memesan_produk`.`total_harga` AS `total_harga`,`memesan_produk`.`jumlah` AS `jumlah`,`memesan_produk`.`id` AS `idpembeli` from (`penjualan_produk` join `memesan_produk` on((`penjualan_produk`.`id_produk` = `memesan_produk`.`id_produk`)))) ;

-- --------------------------------------------------------

--
-- Structure for view `penjualan`
--
DROP TABLE IF EXISTS `penjualan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `penjualan`  AS  (select `produk`.`id_produk` AS `id_produk`,`produk`.`nama_produk` AS `nama_produk`,`produk`.`foto_produk` AS `foto_produk`,`penjualan_produk`.`harga_produk` AS `harga_produk`,`penjualan_produk`.`stok` AS `stok`,`penjualan_produk`.`tanggal_penjualanproduk` AS `tanggal_penjualanproduk` from (`produk` join `penjualan_produk` on((`produk`.`id_produk` = `penjualan_produk`.`id_produk`)))) ;

-- --------------------------------------------------------

--
-- Structure for view `penjualanjasa`
--
DROP TABLE IF EXISTS `penjualanjasa`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `penjualanjasa`  AS  (select `jasa`.`id_jasa` AS `id_jasa`,`jasa`.`id` AS `id`,`jasa`.`nama_jasa` AS `nama_jasa`,`jasa`.`id_kategorijasa` AS `id_kategorijasa`,`penjualan_jasa`.`harga_jasa` AS `harga_jasa`,`penjualan_jasa`.`status_jasa` AS `status_jasa` from (`jasa` join `penjualan_jasa` on((`jasa`.`id_jasa` = `penjualan_jasa`.`id_jasa`)))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_kategorijasa`
--
ALTER TABLE `detail_kategorijasa`
  ADD PRIMARY KEY (`id_detail`),
  ADD KEY `FKthps9h1tmhj9epkksc0ou05sm` (`id_kategorijasa`);

--
-- Indexes for table `jasa`
--
ALTER TABLE `jasa`
  ADD PRIMARY KEY (`id_jasa`),
  ADD KEY `FK78qjob2guij0j0ffwukb8ycwh` (`id_kategorijasa`),
  ADD KEY `FK3f4w3uxpaa1t0iwfp1k31n8j4` (`id`);

--
-- Indexes for table `kategori_jasa`
--
ALTER TABLE `kategori_jasa`
  ADD PRIMARY KEY (`id_kategorijasa`);

--
-- Indexes for table `kategori_produk`
--
ALTER TABLE `kategori_produk`
  ADD PRIMARY KEY (`id_kategoriproduk`);

--
-- Indexes for table `memesan_jasa`
--
ALTER TABLE `memesan_jasa`
  ADD PRIMARY KEY (`id_pemesananjasa`),
  ADD KEY `FK96vn3co4518r4g5ao4ynv77ex` (`id_jasa`),
  ADD KEY `FK2d0ok1a9gw3d2f6y3rp6i8xam` (`id`);

--
-- Indexes for table `memesan_produk`
--
ALTER TABLE `memesan_produk`
  ADD PRIMARY KEY (`id_pemesananproduk`),
  ADD KEY `FKinm81u8qf6tcl2tlecvwtv9x6` (`id_produk`),
  ADD KEY `FKmg5vb3ncctfa8p4iudn4plklb` (`id`);

--
-- Indexes for table `pembayaran_jasa`
--
ALTER TABLE `pembayaran_jasa`
  ADD PRIMARY KEY (`id_pembayaranjasa`),
  ADD KEY `FK2ulmk5rv0tpukw4hfq9772pyp` (`id_jasa`),
  ADD KEY `FK13ryyk0lnitwhnm01hb4nktip` (`id_pemesananjasa`),
  ADD KEY `FKkdoistoel57earp4vyycru5nj` (`id`);

--
-- Indexes for table `pembayaran_produk`
--
ALTER TABLE `pembayaran_produk`
  ADD PRIMARY KEY (`id_pembayaranproduk`),
  ADD KEY `FKd0ifx0sex3vhmr2q4xhnj8jf3` (`id_pemesananproduk`),
  ADD KEY `FKm1rj9xew7i7bb88jr0qsm799c` (`id_produk`),
  ADD KEY `FKt89csundicv3kg5p0yu3hakmx` (`id`);

--
-- Indexes for table `penjualan_jasa`
--
ALTER TABLE `penjualan_jasa`
  ADD PRIMARY KEY (`id_penjualanjasa`),
  ADD KEY `FK6v8g9w76cf7a83rtdeeyegim6` (`id_jasa`),
  ADD KEY `FK6xr7aeeeko86u9us19ipqnvej` (`id`);

--
-- Indexes for table `penjualan_produk`
--
ALTER TABLE `penjualan_produk`
  ADD PRIMARY KEY (`id_penjualanproduk`),
  ADD KEY `FKjxdql9aq4nmivxvnny3a0c909` (`id_produk`),
  ADD KEY `FKo1mxcg56lyqvyvdak4lw0tkxe` (`id`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`),
  ADD KEY `FKpk0edtr3hrw65jyr12d3d5xj8` (`id_kategoriproduk`),
  ADD KEY `FK3tk0fm783gcyh3of5ygmst2ir` (`id`);

--
-- Indexes for table `tinjauan_jasa`
--
ALTER TABLE `tinjauan_jasa`
  ADD PRIMARY KEY (`id_tinjauanjasa`),
  ADD KEY `FKkjds4fs4njhgpbtvg45agjicx` (`id_jasa`),
  ADD KEY `FKnh76mi5fkcgtpmjp0h4kknvx1` (`id`);

--
-- Indexes for table `tinjauan_produk`
--
ALTER TABLE `tinjauan_produk`
  ADD PRIMARY KEY (`id_tinjauanproduk`),
  ADD KEY `FKl916uauwjbt92emc5v2jv2dk8` (`id_produk`),
  ADD KEY `FKjbo0te23xipd3nt2w7d7m50hs` (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_ob8kqyqqgmefl0aco34akdtpe` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_kategorijasa`
--
ALTER TABLE `detail_kategorijasa`
  MODIFY `id_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `jasa`
--
ALTER TABLE `jasa`
  MODIFY `id_jasa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `kategori_jasa`
--
ALTER TABLE `kategori_jasa`
  MODIFY `id_kategorijasa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `kategori_produk`
--
ALTER TABLE `kategori_produk`
  MODIFY `id_kategoriproduk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `memesan_jasa`
--
ALTER TABLE `memesan_jasa`
  MODIFY `id_pemesananjasa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `memesan_produk`
--
ALTER TABLE `memesan_produk`
  MODIFY `id_pemesananproduk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `pembayaran_jasa`
--
ALTER TABLE `pembayaran_jasa`
  MODIFY `id_pembayaranjasa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pembayaran_produk`
--
ALTER TABLE `pembayaran_produk`
  MODIFY `id_pembayaranproduk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `penjualan_jasa`
--
ALTER TABLE `penjualan_jasa`
  MODIFY `id_penjualanjasa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `penjualan_produk`
--
ALTER TABLE `penjualan_produk`
  MODIFY `id_penjualanproduk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id_produk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tinjauan_jasa`
--
ALTER TABLE `tinjauan_jasa`
  MODIFY `id_tinjauanjasa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tinjauan_produk`
--
ALTER TABLE `tinjauan_produk`
  MODIFY `id_tinjauanproduk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `detail_kategorijasa`
--
ALTER TABLE `detail_kategorijasa`
  ADD CONSTRAINT `FKthps9h1tmhj9epkksc0ou05sm` FOREIGN KEY (`id_kategorijasa`) REFERENCES `kategori_jasa` (`id_kategorijasa`);

--
-- Constraints for table `jasa`
--
ALTER TABLE `jasa`
  ADD CONSTRAINT `FK3f4w3uxpaa1t0iwfp1k31n8j4` FOREIGN KEY (`id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK78qjob2guij0j0ffwukb8ycwh` FOREIGN KEY (`id_kategorijasa`) REFERENCES `kategori_jasa` (`id_kategorijasa`);

--
-- Constraints for table `memesan_jasa`
--
ALTER TABLE `memesan_jasa`
  ADD CONSTRAINT `FK2d0ok1a9gw3d2f6y3rp6i8xam` FOREIGN KEY (`id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK96vn3co4518r4g5ao4ynv77ex` FOREIGN KEY (`id_jasa`) REFERENCES `jasa` (`id_jasa`);

--
-- Constraints for table `memesan_produk`
--
ALTER TABLE `memesan_produk`
  ADD CONSTRAINT `FKinm81u8qf6tcl2tlecvwtv9x6` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id_produk`),
  ADD CONSTRAINT `FKmg5vb3ncctfa8p4iudn4plklb` FOREIGN KEY (`id`) REFERENCES `user` (`id`);

--
-- Constraints for table `pembayaran_jasa`
--
ALTER TABLE `pembayaran_jasa`
  ADD CONSTRAINT `FK13ryyk0lnitwhnm01hb4nktip` FOREIGN KEY (`id_pemesananjasa`) REFERENCES `memesan_jasa` (`id_pemesananjasa`),
  ADD CONSTRAINT `FK2ulmk5rv0tpukw4hfq9772pyp` FOREIGN KEY (`id_jasa`) REFERENCES `jasa` (`id_jasa`),
  ADD CONSTRAINT `FKkdoistoel57earp4vyycru5nj` FOREIGN KEY (`id`) REFERENCES `user` (`id`);

--
-- Constraints for table `pembayaran_produk`
--
ALTER TABLE `pembayaran_produk`
  ADD CONSTRAINT `FKd0ifx0sex3vhmr2q4xhnj8jf3` FOREIGN KEY (`id_pemesananproduk`) REFERENCES `memesan_produk` (`id_pemesananproduk`),
  ADD CONSTRAINT `FKm1rj9xew7i7bb88jr0qsm799c` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id_produk`),
  ADD CONSTRAINT `FKt89csundicv3kg5p0yu3hakmx` FOREIGN KEY (`id`) REFERENCES `user` (`id`);

--
-- Constraints for table `penjualan_jasa`
--
ALTER TABLE `penjualan_jasa`
  ADD CONSTRAINT `FK6v8g9w76cf7a83rtdeeyegim6` FOREIGN KEY (`id_jasa`) REFERENCES `jasa` (`id_jasa`),
  ADD CONSTRAINT `FK6xr7aeeeko86u9us19ipqnvej` FOREIGN KEY (`id`) REFERENCES `user` (`id`);

--
-- Constraints for table `penjualan_produk`
--
ALTER TABLE `penjualan_produk`
  ADD CONSTRAINT `FKjxdql9aq4nmivxvnny3a0c909` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id_produk`),
  ADD CONSTRAINT `FKo1mxcg56lyqvyvdak4lw0tkxe` FOREIGN KEY (`id`) REFERENCES `user` (`id`);

--
-- Constraints for table `produk`
--
ALTER TABLE `produk`
  ADD CONSTRAINT `FK3tk0fm783gcyh3of5ygmst2ir` FOREIGN KEY (`id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FKpk0edtr3hrw65jyr12d3d5xj8` FOREIGN KEY (`id_kategoriproduk`) REFERENCES `kategori_produk` (`id_kategoriproduk`);

--
-- Constraints for table `tinjauan_jasa`
--
ALTER TABLE `tinjauan_jasa`
  ADD CONSTRAINT `FKkjds4fs4njhgpbtvg45agjicx` FOREIGN KEY (`id_jasa`) REFERENCES `jasa` (`id_jasa`),
  ADD CONSTRAINT `FKnh76mi5fkcgtpmjp0h4kknvx1` FOREIGN KEY (`id`) REFERENCES `user` (`id`);

--
-- Constraints for table `tinjauan_produk`
--
ALTER TABLE `tinjauan_produk`
  ADD CONSTRAINT `FKjbo0te23xipd3nt2w7d7m50hs` FOREIGN KEY (`id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FKl916uauwjbt92emc5v2jv2dk8` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id_produk`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
