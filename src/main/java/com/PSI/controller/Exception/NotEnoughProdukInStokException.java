//Kelompok 06_PSI_2018
package com.PSI.controller.Exception;
import com.PSI.model.PenjualanProduk;
public class NotEnoughProdukInStokException extends Exception{
    private PenjualanProduk produk;
    private static final String DEFAULT_MESSAGE = "Not enough products in stock";

    public NotEnoughProdukInStokException() {
        super(DEFAULT_MESSAGE);
    }

    public NotEnoughProdukInStokException(PenjualanProduk produk) {
        super("Produk ini tidak dapat dibeli" + " products in stock Only " +produk.getStok()+ " left");
        this.produk = produk;
    }
}

