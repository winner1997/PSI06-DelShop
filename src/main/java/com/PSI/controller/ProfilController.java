 //Kelompok 06_PSI_2018
package com.PSI.controller;
import com.PSI.service.ProfilService;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.PSI.model.Produk;
import com.PSI.model.User;
import com.PSI.repository.UserRepository;

@Controller
public class ProfilController {
	private ProfilService PS;
	
	@Autowired
	private UserRepository us;
	User useraktif;
	@Autowired
	private UserRepository ur;
	
	 @RequestMapping(value = "/profiluser/{id}", method = RequestMethod.GET)
	    public String profil(@PathVariable Integer id,Model model , User user) {
		 	model.addAttribute("profiluser", us.getOne(user.getId()));
	    	return "/profiluser/profil";
	    }
	 
	@RequestMapping(value="/profiluser/{id}", method = RequestMethod.POST)
	public String editProfil(Model model,User user,@PathVariable int id,HttpServletRequest req, 
			@RequestParam("fotoprofil") MultipartFile foto){
		String namadepan = req.getParameter("namaDepan");
		String namabelakang = req.getParameter("namaBelakang");
		user = ur.findById(id);
		if (!foto.isEmpty()) {
			user.setFotoProfil(foto.getOriginalFilename());
			String name = foto.getOriginalFilename();
			try {
				byte[] bytes = foto.getBytes();
				BufferedOutputStream stream = new BufferedOutputStream(
				new FileOutputStream(new File("D:/DS/src/main/resources/static/fotoprofil/"+name)));
				stream.write(bytes);
				stream.close();
				System.out.println(stream);
			} catch (Exception e) {
				return "Gagal mengupload " + name + " => " + e.getMessage();
			}
		}
		int id2 =id; 
		user.setNamaBelakang(namabelakang);
		user.setNamaDepan(namadepan);
		us.save(user);
		return "redirect:/profiluser/"+id2;		
	}
}