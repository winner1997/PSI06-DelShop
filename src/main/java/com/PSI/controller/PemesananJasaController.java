package com.PSI.controller;

import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.PSI.Impl.JasaServiceImpl;
import com.PSI.model.Jasa;
import com.PSI.model.MemesanJasa;
import com.PSI.model.User;
import com.PSI.repository.JasaRepository;
import com.PSI.repository.MemesanJasaRepository;
import com.PSI.repository.UserRepository;
import com.PSI.repository.ViewPenjualanJasaRepository;
import com.PSI.service.MemesanJasaService;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
@Controller
public class PemesananJasaController{
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	Date date = new Date();
	User useraktif;
	UserRepository ur;
	private JasaServiceImpl js;
	MemesanJasaService penjualanjasa;
	@Autowired
	private MemesanJasaRepository mj;
	@Autowired
	private ViewPenjualanJasaRepository vm;
	@Autowired
	private JasaRepository jr;
	@GetMapping("/pemesananjasa/cart")
	public ModelAndView shoppingCart() {
		ModelAndView modelAndView = new ModelAndView("PenjualanCart");
	    modelAndView.addObject("jasa", penjualanjasa.getProductsInCart());
	    modelAndView.addObject("total", penjualanjasa.getTotal().toString());
	    return modelAndView;
	}
	@GetMapping("/jasa/tambahjasa/{id_jasa}")
	public ModelAndView addProductToCart(@PathVariable int id_jasa) {
	    penjualanjasa.addPenjualan(js.findJasaByIdjasa(id_jasa));
	    return shoppingCart();
	}
	@GetMapping("/jasa/hapusjasa/{id_jasa}")
	public ModelAndView removeProductFromCart(@PathVariable int id_jasa) {        
		penjualanjasa.removePenjualan(js.findJasaByIdjasa(id_jasa));
	    return shoppingCart();
	}    
	@GetMapping("/jasa/checkout")
    public ModelAndView checkout() {
        penjualanjasa.checkout();
        return shoppingCart();
	}
	@RequestMapping(value ="/pemesananjasa/{id}",method = RequestMethod.POST)
	public String SavePemesanan(Model model,@PathVariable int id,HttpServletRequest req,MemesanJasa mesanjasa,
			@RequestParam ("catatan") String catatan) {
			useraktif =(User) req.getSession().getAttribute("user");
			Jasa jasa = jr.findOne(id);
		 	mesanjasa.setCatatan(catatan);
			mesanjasa.setJasa(jasa);
			mesanjasa.setUser(jasa.getUser());
			mesanjasa.setIdpemesan(useraktif.getId());
			mesanjasa.setStatusPemesananJasa("Menunggu Pembayaran");
			mesanjasa.setTanggalTerimajasa(dateFormat.format(date));
			mj.save(mesanjasa);
		return "redirect:/index";
	}
	@RequestMapping(value="keranjangjasa/{id}", method = RequestMethod.GET)
	public String keranjangJasa(Model model , HttpServletRequest req,@PathVariable int id) {
		User user = (User) req.getSession().getAttribute("user");
		if(jr.findOne(id).getUser().getId()==user.getId()) {
			return "redirect:/index";
		}
		model.addAttribute("pesanjasa", vm.findOne(id));
		model.addAttribute("jasa", jr.findOne(id));
		return "pemesananjasa/keranjang_jasa";
	}
}	