package com.PSI.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.PSI.model.PemesananMenunggu;
import com.PSI.model.User;
import com.PSI.repository.MemesanJasaRepository;
import com.PSI.repository.PembayaranJasaRepository;
import com.PSI.repository.PembayaranProdukRepository;
import com.PSI.repository.PemesananMenungguRepository;
import com.PSI.repository.PemesananMenungguUserRepository;
import com.PSI.service.PemesananMenungguService;

@Controller
public class RiwayatController {
	@Autowired
	private PemesananMenungguRepository men;
	private PemesananMenungguService me;
	@Autowired
	private PembayaranProdukRepository ppr;
	@Autowired
	private MemesanJasaRepository mjr;
	@Autowired
	private PembayaranJasaRepository pjr;
	
	//riwayat pembelian user
	@RequestMapping(value = "/riwayatproduk/{id}", method = RequestMethod.GET)
	public String riwayat(Model model ,HttpServletRequest req,@PathVariable Integer id) {
		model.addAttribute("riwayat",men.findByidpembeli(id));
			return "riwayat";
	}
	//riwayat produk yang dibeli orang
	@RequestMapping(value = "/pembayaran/{id}" , method = RequestMethod.GET)
	public String riwayatUser(Model model , HttpServletRequest req, @PathVariable int id) {
			model.addAttribute("pembayaran", ppr.findAll());
		return "laporan-penjualan";
	}
	
	
	@RequestMapping(value = "riwayatjasa/{id}", method = RequestMethod.GET)
	public String riwayatJasa(Model model , HttpServletRequest req, @PathVariable Integer id) {
			model.addAttribute("riwayat", mjr.findByidpemesan(id));
		return "riwayatjasa";
	}
	@RequestMapping(value = "riwayatpembelian",method = RequestMethod.GET)
	public String riwayatPembelian(Model model , HttpServletRequest req ) {
			User user = (User) req.getSession().getAttribute("user");
			model.addAttribute("pembayaran",pjr.findByUser(user));
		return "laporan-penjualanjasa";
	}
}
