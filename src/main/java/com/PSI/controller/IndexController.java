//Kelompok 06_PSI_2018
package com.PSI.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.PSI.model.Penjualan;
import com.PSI.service.PenjualanService;
import com.PSI.util.Pager;


@Controller
public class IndexController {
	
	private static final int INITIAL_PAGE=0;
	private final PenjualanService penjualanService;
	
	@Autowired
	public IndexController(PenjualanService penjualanService) {
		this.penjualanService=penjualanService;
	}
	
    @GetMapping(value="/index")
    public ModelAndView home(@RequestParam("page") Optional<Integer> page) {
    	int evalPage = (page.orElse(0) < 1 ? INITIAL_PAGE : page.get()-1);	
        ModelAndView modelAndView = new ModelAndView();
        Page <Penjualan> products = penjualanService.findAllProductsPageable(new PageRequest(evalPage,6));
        Pager pager = new Pager(products);
    	modelAndView.addObject("listProduk",products);
    	modelAndView.addObject("pager", pager);
        modelAndView.setViewName("/index");
        return modelAndView;
    }    
    
    @GetMapping("/indexjasa")
    public ModelAndView homejasa(@RequestParam("pageSize") Optional<Integer> pageSize,
            @RequestParam("page") Optional<Integer> page) {
    	ModelAndView modelAndView = new ModelAndView();
    	modelAndView.setViewName("jasa/index_jasa");
    	return modelAndView;
    }
    
    
    @GetMapping("/kategorijasa")
    public ModelAndView kategorijasa(@RequestParam("pageSize") Optional<Integer> pageSize,
            @RequestParam("page") Optional<Integer> page) {
    	ModelAndView modelAndView = new ModelAndView();
    	modelAndView.setViewName("jasa/sub_kategori_jasa");
    	return modelAndView;
    } 
}