package com.PSI.controller;
import java.math.BigDecimal;
import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.PSI.model.DetailKategoriJasa;
import com.PSI.model.Jasa;
import com.PSI.model.KategoriJasa;
import com.PSI.model.PenjualanJasa;
import com.PSI.model.User;
import com.PSI.model.ViewPenjualanJasa;
import com.PSI.repository.DetailKategoriJasaRepository;
import com.PSI.repository.JasaRepository;
import com.PSI.repository.KategoriJasaRepository;
import com.PSI.repository.PenjualanJasaRepository;
import com.PSI.repository.UserRepository;
import com.PSI.repository.ViewPenjualanJasaRepository;
import com.PSI.service.JasaService;
import com.PSI.service.PenjualanJasaService;

@Controller
public class JasaController {
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	Date date = new Date();
	
	Jasa aktif;
	private JasaService jser;
	private PenjualanJasaService j;
	
	@Autowired
	UserRepository ur;
	@Autowired
	private JasaRepository JR;
	@Autowired
	private PenjualanJasaRepository ppj;
	@Autowired
	private JasaRepository jas;
	@Autowired
	private KategoriJasaRepository dj;
	@Autowired
	private ViewPenjualanJasaRepository v;
	@RequestMapping(value="tambah/jasa/{id}" , method = RequestMethod.POST)
	public String tambah(@PathVariable Integer id, @Valid PenjualanJasa penjualanjasa, @Valid Jasa jasa,
				@RequestParam("deskripsijasa") String deskripsi,
				@RequestParam("hargajasa") BigDecimal harga,
				@RequestParam("jenisjasa") String jenisjasa,
				@RequestParam("namajasa") String namajasa){
			User useraktif = ur.findById(id);
			
			//jasa = JR.findByIdJasa(jasa.getIdJasa());
			KategoriJasa kategori = dj.findByNamaKategorijasa(jenisjasa);
			jasa.setUser(useraktif);
			jasa.setDeskripsiJasa(deskripsi);
			jasa.setKategorijasa(kategori.getIdKategoriJasa());
			jasa.setNama_jasa(namajasa); 
			JR.save(jasa);
			penjualanjasa.setHargaJasa(harga);
			penjualanjasa.setStatusJasa("Ready");
			Jasa jasas = jas.findOne(jasa.getIdJasa());
			penjualanjasa.setJasa(jasas);
			penjualanjasa.setUser(useraktif);			
			ppj.save(penjualanjasa);
		return "redirect:/index";
	}
		@RequestMapping(value = "/get/penjualanjasa/{idJasa}", method = RequestMethod.GET)
		public String getJasa (Model model ,@PathVariable int idJasa) {
			model.addAttribute("get", ppj.findOne(idJasa));
			return "/jasa/rincian_jasa";
		}
		
		@RequestMapping(value = "/edit/PenjualanJasa/", method = RequestMethod.POST)
		public String editJasa (Model model, Principal l, PenjualanJasa jasa,
				@RequestParam("hargajasa") BigDecimal harga,
				@RequestParam("status") String status) {
//			jasa = ppj.findByIdPenjualanJasa(idJasa);
			jasa.setStatusJasa(status);
			jasa.setHargaJasa(harga);
			model.addAttribute("editjasa", j.editPenjualan(jasa));
			return "redirect:/jasa/edit-jasa";
		}
		
		@RequestMapping(value = "/hapus/PenjualanJasa/{id}", method = RequestMethod.GET)
		public String hapusJasa (@PathVariable int id,Model model, Principal l, PenjualanJasa penjualanjasa,Jasa jasa) {
			penjualanjasa = ppj.findByIdPenjualanJasa(id);
			//jasa = JR.findByIdJasa(id);
			j.hapusPenjualan(penjualanjasa);
			j.hapusJasa(jasa);
			return "redirect:/jasa/index";
		}
		
		@RequestMapping(value="/detail/view/jasa/{id}" , method = RequestMethod.GET)
		public String detailJasa (@PathVariable int id, Model model,Principal l, PenjualanJasa jasa) {
			model.addAttribute("detailpenjualan",j.detailPenjualan(id));
			model.addAttribute("detailjasa", j.detailJasa(id));
			return "redirect:/jasa/detail-jasa";
		}
		@RequestMapping(value="/jualjasa/{id}" , method = RequestMethod.GET)
		public String listJasa (Model model,@PathVariable int id, HttpServletRequest req) {
			User user = (User) req.getSession().getAttribute("user");
			model.addAttribute("listJasa", JR.findByUser(user));
			return "jasa/jualjasa";
		}
		@RequestMapping("/tambahjasa")
		public String tambahjasa() {
			return "jasa/tambah_jasa";
		}
		@GetMapping("/rincianjasa")
	    public ModelAndView detailJasa(@RequestParam("pageSize") Optional<Integer> pageSize,
	                             @RequestParam("page") Optional<Integer> page) {
	        ModelAndView modelAndView = new ModelAndView();
	        modelAndView.setViewName("jasa/rincian_jasa");
	        return modelAndView;
	    }
		
		@RequestMapping(value = "/subjasa/{id}", method = RequestMethod.GET)
	    public String subkategoriJasa1(Model model,@PathVariable Integer id) {
			model.addAttribute("listjasa",  v.findByIdkategorijasa(id));
	        return "jasa/rincian_jasa";
	    }
}