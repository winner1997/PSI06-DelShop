//Kelompok 06_PSI_2018
package com.PSI.controller;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.support.RequestPartServletServerHttpRequest;
import org.springframework.web.servlet.ModelAndView;

import com.PSI.Impl.ProdukServiceImpl;
import com.PSI.controller.Exception.NotEnoughProdukInStokException;
import com.PSI.model.MemesanProduk;
import com.PSI.model.Penjualan;
import com.PSI.model.Produk;
import com.PSI.model.User;
import com.PSI.repository.MemesanProdukRepository;
import com.PSI.repository.PenjualanRepository;
import com.PSI.repository.ProdukRepository;
import com.PSI.repository.UserRepository;
import com.PSI.repository.WinnerRepository;
import com.PSI.service.MemesanProdukService;

@Controller
public class PemesananProdukController {
	@Autowired
	private UserRepository ur;
	private ProdukServiceImpl PS;
	@Autowired
    MemesanProdukService PenjualanProduk;
	@Autowired
	private MemesanProdukRepository mp;
	@Autowired
	private ProdukRepository pr;
	@Autowired
	private WinnerRepository prepo;
	
	@RequestMapping(value="/keranjangproduk/{id}", method = RequestMethod.GET)
	public String pesanAnda(Model model,@PathVariable int id , HttpServletRequest req) {
		User user1 = (User) req.getSession().getAttribute("user");
		Produk pro = pr.findOne(id);
		if(pro.getUser().getId()==user1.getId()) {
			return "redirect:/index";
		}else {
			model.addAttribute("pesan", prepo.findOne(id));
			return "pemesananproduk/Keranjang";	
		}
	}

	@GetMapping("/keranjang")
    public ModelAndView shoppingCart(){
        ModelAndView modelAndView = new ModelAndView("/pemesananproduk/keranjang");
        modelAndView.addObject("produk", PenjualanProduk.getProductsInCart());
        modelAndView.addObject("total", PenjualanProduk.getTotal().toString());
        modelAndView.addObject("penjualan",PenjualanProduk.getPenjualanProduk());
        modelAndView.setViewName("pemesananproduk/keranjang");
        return modelAndView;
    }
	
    @GetMapping("/addproduk/{id}")
    public ModelAndView addProductToCart(@PathVariable("id") int id) {
        PenjualanProduk.addPenjualan(pr.findOne(id));
        return shoppingCart();
    }
    @GetMapping("/removeProduct/{id}")
    public ModelAndView removeProductFromCart(@PathVariable("id") int id) {        
		PenjualanProduk.removePenjualan(PS.findByIdProduk(id));
        return shoppingCart();
    }
    @GetMapping("/produkCart/checkout")
    public ModelAndView checkout() {
        try {
            PenjualanProduk.checkout();
        } catch (NotEnoughProdukInStokException e) {
            return shoppingCart().addObject("outOfStockMessage", e.getMessage());
        }
        return shoppingCart();
    }    
    
    @RequestMapping(value = "pesanproduk/{id}", method = RequestMethod.POST)
    public String mesanproduk(HttpServletRequest req,@PathVariable int id , Produk produk, MemesanProduk pesan,
    		@RequestParam ("ciriproduk") String ciri,
    		@RequestParam ("jumlah") int jumlah) {
		User user = (User) req.getSession().getAttribute("user");	
		produk = pr.findOne(id);	
		Penjualan penjualan = prepo.findOne(id);
		pesan.setCiriProduk(ciri);
		pesan.setJumlah(jumlah);
		pesan.setUser(user);
		pesan.setProduk(produk);
		pesan.setStatus("Menunggu Pembayaran");
		int harga = penjualan.getHargaProduk();
		pesan.setTotalHarga(harga*jumlah);
		mp.save(pesan);
    	return "redirect:/index";
    }
    
    @GetMapping("/keranjangjasa")
    public ModelAndView keranjangjasa(@RequestParam("pageSize") Optional<Integer> pageSize,
                             @RequestParam("page") Optional<Integer> page) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("pemesananjasa/keranjang_jasa");
        return modelAndView;
    }

    
    @RequestMapping(value = "/produk/terimapemesanan/{id_produk}",method = RequestMethod.POST)
    public String terimaPesanan (@PathVariable int id_produk, MemesanProduk mesan) {
    	mesan = mp.findByIdPemesananProduk(id_produk);
    	mesan.setStatus("Selesai");
    	return "/riwayat";
    }
    @RequestMapping(value = "/produknunggu/{id}", method = RequestMethod.GET)
    public String GetRiwayatMenunggu(Model model ,@PathVariable int id, MemesanProduk mesan) {
    	model.addAttribute("riwayatproduk", mp.findByStatusMenunggu("Menunggu Konfirmasi", ur.findById(id)));
    	return "/riwayatmenunggu";
    }
    @RequestMapping(value = "/produkselesai/{id}", method = RequestMethod.GET)
    public String GetRiwayatSelesai(Model model ,@PathVariable int id, MemesanProduk mesan) {
    	model.addAttribute("riwayatproduk", mp.findByStatusMenunggu("Selesai", ur.findById(id)));
    	return "/riwayatselesai";
    }
}