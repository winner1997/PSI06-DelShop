//Kelompok 06_PSI_2018
package com.PSI.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.hibernate.annotations.GenerationTime;
import org.omg.PortableInterceptor.SUCCESSFUL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.PSI.model.*;
import com.PSI.repository.JasaRepository;
import com.PSI.repository.KategoriJasaRepository;
import com.PSI.repository.KategoriProdukRepository;
import com.PSI.repository.PenjualanJasaRepository;
import com.PSI.repository.PenjualanProdukRepository;
import com.PSI.repository.PenjualanRepository;
import com.PSI.repository.ProdukRepository;
import com.PSI.repository.UserRepository;
import com.PSI.service.PenjualanProdukService;
import com.PSI.service.ProdukService;
import com.PSI.Impl.PenjualanJasaServiceImpl;
import com.PSI.Impl.PenjualanProdukServiceImpl;
import com.PSI.controller.LoginController;
@Controller
public class ProdukController {
	User useraktif;
	Produk paktif;
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	Date date = new Date();
	@Autowired
	UserRepository ur;
	@Autowired
	private PenjualanProdukServiceImpl p;
	@Autowired
	private PenjualanProdukRepository pps;
	@Autowired
	private ProdukRepository pr;
	@Autowired
	KategoriProdukRepository kpr;
	//Produk
	private ProdukService ps;
	@Autowired
	private PenjualanRepository pen;
	@RequestMapping(value = "/tambahproduk")
	public String baru() {
		return "produk/tambah_produk";
	}
	
	@RequestMapping(value = "/jualproduk/{id}", method = RequestMethod.GET)
	public String listProduk (Model model,@PathVariable int id , HttpServletRequest req) {
		User user = (User) req.getSession().getAttribute("user");
		model.addAttribute("listProduk",pr.findByUser(user));
		return "/produk/jualproduk";
	}
	@RequestMapping(value = "/tambah/produk/{id}", method = RequestMethod.POST)
	public String tambahProduk (@PathVariable Integer id,Model model,@Valid Produk produk, @Valid PenjualanProduk penjualanProduk,Principal l,KategoriProduk kategoriproduk,
			 @RequestParam("fotoproduk") MultipartFile file,
			 @RequestParam("ciriproduk") String ciri,
			 @RequestParam("deskripsi") String deskripsi,
			 @RequestParam("namaproduk") String namaproduk,
			 @RequestParam("idkategori") int kategori,
			 @RequestParam("harga") BigDecimal harga,
			 @RequestParam("stok") int stok){
		useraktif = ur.findById(id);
		if (!file.isEmpty()) {
			produk.setFotoProduk(file.getOriginalFilename());
			String name = file.getOriginalFilename();
			try {
				byte[] bytes = file.getBytes();
				BufferedOutputStream stream = new BufferedOutputStream(
				new FileOutputStream(new File("E:/Materi Kuliah/Semester 6/PROSI/contoh/DS/src/main/resources/static/gambar"+name)));
				stream.write(bytes);
				stream.close();
				System.out.println(stream);
			} catch (Exception e) {
				return "Gagal mengupload " + name + " => " + e.getMessage();
			}
		}
		kategoriproduk = kpr.findByIdKategoriProduk(kategori);
		produk.setDeskripsiProduk(deskripsi);
		produk.setCiriProduk(ciri);
		produk.setNamaProduk(namaproduk);
		produk.setKategoriproduk(kategoriproduk);
		produk.setUser(useraktif);
		pr.save(produk);
		penjualanProduk.setUser(useraktif);
		penjualanProduk.setHargaProduk(harga);
		penjualanProduk.setStok(stok);
		penjualanProduk.setStatusProduk("Ready");
		penjualanProduk.setTanggalPenjualanProduk(dateFormat.format(date));
		Produk prod = pr.findOne(produk.getIdProduk());
		penjualanProduk.setProduk(prod);
		pps.save(penjualanProduk);
		return "redirect:/index";
	}
	@RequestMapping(value = "/editproduk/{id}", method = RequestMethod.GET)
	public String edit () {
		return "/produk/edit_produk";
	}
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public String editProduk (Model model,@PathVariable int id,PenjualanProduk produk,Principal l,
			@RequestParam("harga") BigDecimal harga,
			@RequestParam("stok") int stok,
			@RequestParam("status") String status) {
		produk = pps.findOne(id);
		produk.setHargaProduk(harga);
		produk.setStok(stok);
		produk.setStatusProduk(status);
		pps.save(produk);
		return "return:/jualproduk";
	}
	
	@RequestMapping(value = "/hapus/{id}",method = RequestMethod.GET)
	public String hapusProduk (@PathVariable int id,Model model , PenjualanProduk penjualanproduk, Principal l,Produk produk) {
		penjualanproduk = pps.findOne(id);
		produk = pps.findOne(id).getProduk();
		pps.delete(penjualanproduk);
		pr.delete(produk);
		return "redirect:/jualproduk";
	}
	@RequestMapping(value = "/rincianproduk/{id}", method = RequestMethod.GET)
	public String detailProduk (@PathVariable int id,Model model , Penjualan produk) {
		//List <User> user = penjualanRepo.findByIdProduk(prod);
        model.addAttribute("revprod", pr.findOne(id)); // View Hasil
        model.addAttribute("rev", pen.findByIdProduk(id));
        model.addAttribute("user", ur.findById(id));
        return "produk/rincian_produk";
	}
}