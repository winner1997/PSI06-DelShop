package com.PSI.controller;
import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.PSI.service.PembayaranJasaService;
import com.PSI.model.MemesanJasa;
import com.PSI.model.PembayaranJasa;
import com.PSI.model.User;
import com.PSI.repository.JasaRepository;
import com.PSI.repository.MemesanJasaRepository;
import com.PSI.repository.PembayaranJasaRepository;
import com.PSI.repository.UserRepository;

@Controller
public class PembayaranJasaController{
	User useraktif;
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	Date date = new Date();
	@Autowired
	UserRepository ur;
	private PembayaranJasaService pe;
	@Autowired
	private MemesanJasaRepository ja;
	@Autowired
	private PembayaranJasaRepository k;
	
	@RequestMapping(value = "/pembayaranjasa/{id}", method = RequestMethod.POST)
	public String pembayaranJasa(@PathVariable int id,Model model,PembayaranJasa Pjasa,HttpServletRequest req,
			@RequestParam ("metode") String metode,
			@RequestParam ("rincian") String rincian) {
			MemesanJasa j = ja.findOne(id);
;			useraktif = (User) req.getSession().getAttribute("user");
			Pjasa.setRincianPembayaranJasa(rincian);
			Pjasa.setJasa(j.getJasa());
			Pjasa.setMemesanjasa(j);
			Pjasa.setMetodePembayaran(metode);
			Pjasa.setStatusPembayaranJasa("Menunggu Konfirmasi Pembayaran");
			Pjasa.setTanggalPembayaran(dateFormat.format(date));
			Pjasa.setUser(useraktif);
			j.setStatusPemesananJasa("Dibayar");
			ja.save(j);
			k.save(Pjasa);
		return "redirect:/index";
	}
	@RequestMapping("/pembayaranj/{id}")
	public String riwayatPembayaran(Model model,@PathVariable int id) {
		model.addAttribute("pembayaran", ja.findOne(id));
		return "/pembayaran/pemb_jasa";
	}
	@RequestMapping(value = "konfirmasijasa/{id}", method = RequestMethod.GET)
	public String konfrimasiJasa(Model model,@PathVariable int id) {
		PembayaranJasa bayar = k.findOne(id);
		bayar.setStatusPembayaranJasa("Selesai");
		return "redirect:/index";
	}
}