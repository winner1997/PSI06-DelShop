//Kelompok 06_PSI_2018
package com.PSI.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.PSI.repository.JasaRepository;
import com.PSI.repository.PenjualanRepository;
import com.PSI.repository.ProdukRepository;
import com.PSI.repository.TinjauanJasaRepository;
import com.PSI.repository.TinjauanProdukRepository;
import com.PSI.model.Jasa;
import com.PSI.model.Produk;
import com.PSI.model.TinjauanJasa;
import com.PSI.model.TinjauanProduk;
import com.PSI.model.User;
@Controller
public class ReviewController {
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	Date date = new Date();
	@Autowired
	private TinjauanProdukRepository tpr;
	@Autowired
	private ProdukRepository prepo;
	@Autowired
	private PenjualanRepository penjualanRepo;
	@Autowired
	private JasaRepository jrepo;
	@Autowired
	private TinjauanJasaRepository tjr;
	
	@RequestMapping(value="/review/{id}", method = RequestMethod.GET)
    public String reviewProduk(@PathVariable int id, Model model) {
		Produk prod = prepo.findOne(id);
		List<Produk> pro = prepo.findByIdProduk(id);
		List<TinjauanProduk> ti = tpr.findByProduk(prod);
		//List <User> user = penjualanRepo.findByIdProduk(prod);
        model.addAttribute("revprod", pro); // View Hasil
        model.addAttribute("form", new TinjauanProduk()); // View Form
        model.addAttribute("rev", ti);
         return "tinjauan/review_produk";
    }
	@RequestMapping(value="/buatreview/{id}", method = RequestMethod.POST)
    public String reviewProduks(@PathVariable int id, Model model, HttpServletRequest req, TinjauanProduk tinja) {
		Produk prod = prepo.findOne(id);
		User users = (User) req.getSession().getAttribute("user");
		tinja.setUser(users);
		tinja.setProduk(prod);
		tinja.setTanggalTinjauanProduk(dateFormat.format(date));
		tpr.save(tinja);
        return "redirect:/review/{id}";
    }
	@RequestMapping(value="/reviewjasa/{id}", method = RequestMethod.GET)
    public String reviewJasa(@PathVariable int id, Model model) {
		Jasa prod = jrepo.findOne(id);
		List<Jasa> pro = jrepo.findByIdJasa(id);
		List<TinjauanJasa> ti = tjr.findByJasa(prod);
		//List <User> user = penjualanRepo.findByIdProduk(prod);
        model.addAttribute("revprod", pro); // View Hasil
        model.addAttribute("form", new TinjauanJasa()); // View Form
        model.addAttribute("rev", ti);

        return "tinjauan/review_jasa";
    }
	
	@RequestMapping(value="/jasareview/{id}", method = RequestMethod.POST)
    public String reviewJasa(@PathVariable int id, Model model, HttpServletRequest req, TinjauanJasa tinja) {
		Jasa prod = jrepo.findOne(id);
		User users = (User) req.getSession().getAttribute("user");
		tinja.setUser(users);
		tinja.setJasa(prod);
		tinja.setTanggalTinjauan(dateFormat.format(date));
		tjr.save(tinja);
        return "redirect:/reviewjasa/{id}";
    }
	
	
	
	
}
