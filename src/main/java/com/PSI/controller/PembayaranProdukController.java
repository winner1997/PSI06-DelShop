package com.PSI.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.PSI.model.MemesanProduk;
import com.PSI.model.PembayaranProduk;
import com.PSI.model.Produk;
import com.PSI.model.User;
import com.PSI.repository.MemesanProdukRepository;
import com.PSI.repository.PembayaranProdukRepository;
import com.PSI.repository.PembayaranRepository;
import com.PSI.repository.PemesananMenungguRepository;
import com.PSI.repository.ProdukRepository;

@Controller
public class PembayaranProdukController {
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	Date date = new Date();
	@Autowired
	private ProdukRepository prepo;
	@Autowired
	private MemesanProdukRepository mpr;
	@Autowired
	private PembayaranRepository pr;
	@Autowired
	private PembayaranProdukRepository p;
	
	@RequestMapping(value ="/pembayaranproduk/{id}" , method = RequestMethod.GET)
    public String pembayaranProduk(@PathVariable int id, Model model) {
		model.addAttribute("pembayaran", pr.findByidPemesananProduk(id));
        return "pembayaran/pemb_produk";
    }
	@RequestMapping(value = "/bayarproduk/{id}",method=RequestMethod.POST)
	public String bayarProduk(HttpServletRequest req,@PathVariable Integer id,Model model, PembayaranProduk bayar,
			@RequestParam ("metode") String metode,
			@RequestParam ("detailpengiriman") String kirim) {
		User user =  (User) (req.getSession().getAttribute("user"));
		MemesanProduk mesan = mpr.findOne(id);
		Produk prod = prepo.findOne(mesan.getProduk().getIdProduk());
		bayar.setTanggalPembayaran(dateFormat.format(date));
		bayar.setUser(user);
		bayar.setStatus_pembayaran("Menunggu Pengantaran Barang");
		bayar.setProduk(prod);
		bayar.setMetodePembayaran(metode);
		bayar.setDetailPembayaranProduk(kirim);
		bayar.setMemesanproduk(mesan);
		mesan.setStatus("Dibayar");
		p.save(bayar);
		mpr.save(mesan);
		return "redirect:/index";
	}
	@RequestMapping(value = "/konfirmasi/{id}",method = RequestMethod.GET)
	public String Konfirmasi(@PathVariable Integer id) {
		PembayaranProduk pem = p.findOne(id);
		pem.setStatus_pembayaran("Selesai");
		p.save(pem);
		return "redirect:/index";
	}
}
