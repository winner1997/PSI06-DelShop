//Kelompok 06_PSI_2018
package com.PSI.controller;
import com.PSI.service.LoginService;
import com.PSI.service.UserService;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.PSI.model.User;
import com.PSI.repository.UserRepository;
@Controller

public class LoginController {
	UserService Ss;	
    UserRepository uRepo;
    LoginService loginService;
    
    public LoginService getLoginService() {
		return loginService;
	}
    @Autowired
	public void setLoginService(LoginService loginService) {
		this.loginService = loginService;
	}    
    @RequestMapping("/")
    public String login() {
    	return "login";
    }
	@RequestMapping(value="/user/login" , method=RequestMethod.POST)
    public String loginPost(HttpServletRequest request,Model model) { 
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		User user = loginService.login(email, password);
		if(user==null) {
			model.addAttribute("loginError", true);
			return "redirect:/";
		}else {
			request.getSession().setAttribute("user", user);
		}
        return "redirect:/index";
    }
}