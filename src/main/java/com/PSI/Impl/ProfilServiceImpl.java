package com.PSI.Impl;
import com.PSI.repository.UserRepository;
import com.PSI.model.User;
import com.PSI.service.ProfilService;
public class ProfilServiceImpl implements ProfilService{
	private UserRepository up;
	@Override
	
	public User profil(int id) {
		return up.findById(id);
	}
	@Override
	public User editProfil(User profil) {
		return up.save(profil);
	}	
}
