package com.PSI.Impl;

import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import com.PSI.model.Penjualan;
import com.PSI.model.PenjualanProduk;
import com.PSI.model.Produk;
import com.PSI.model.User;
import com.PSI.repository.PenjualanProdukRepository;
import com.PSI.repository.ProdukRepository;
import com.PSI.service.PenjualanProdukService;
@Service
public class PenjualanProdukServiceImpl implements PenjualanProdukService{
	@Autowired
	private EntityManagerFactory emf;
	public void setEmf(EntityManagerFactory emf) {
		this.emf = emf;
	}
	@Autowired
	private ProdukRepository pRepo;
	@Autowired
	private PenjualanProdukRepository ppRepo;
	@Autowired
	private ProdukRepository pr;
	@Override
	public void tambahProduk(Produk produk) {
		pRepo.save(produk);
	}
	@Override
	public void tambahPenjualan(PenjualanProduk produk) {
		ppRepo.save(produk);
	}	

	@Override
	public PenjualanProduk editPenjualan(PenjualanProduk penjualanProduk) {
		return ppRepo.save(penjualanProduk);
	}

	@Override
	public void hapusPenjualan(PenjualanProduk penjualanProduk) {
		ppRepo.delete(penjualanProduk);
	}
	@Override
	public PenjualanProduk detailPenjualan(int id) {
		return ppRepo.findOne(id);
	}
	@Override
	public Produk detailProduk(int id) {
		// TODO Auto-generated method stub
		return pr.getOne(id);
	}
	@Override
	public List<Penjualan> getAllProduk() {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("from Penjualan", Penjualan.class).getResultList();
	}
	@Override
	public List<PenjualanProduk> getAllPenjualan() {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("from PenjualanProduk", PenjualanProduk.class).getResultList();
	}	
}
