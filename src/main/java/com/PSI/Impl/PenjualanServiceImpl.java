package com.PSI.Impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.PSI.model.Penjualan;
import com.PSI.repository.PenjualanRepository;
import com.PSI.service.PenjualanService;
@Service
public class PenjualanServiceImpl implements PenjualanService{

	private final PenjualanRepository penjualanRepository;	
	@Autowired
	public PenjualanServiceImpl(PenjualanRepository penjualanRepository) {
		this.penjualanRepository = penjualanRepository;
	}
	@Override
	public Optional<Penjualan> findByIdProduk(int id) {
		return penjualanRepository.findByIdProduk(id);
	}

	@Override
	public Page<Penjualan> findAllProductsPageable(Pageable pageable) {
		return penjualanRepository.findAll(pageable);
	}
	
}
