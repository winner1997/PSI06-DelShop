package com.PSI.Impl;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import com.PSI.model.Jasa;
import com.PSI.model.MemesanJasa;
import com.PSI.model.PenjualanJasa;
import com.PSI.model.Produk;
import com.PSI.repository.PenjualanJasaRepository;
import com.PSI.service.MemesanJasaService;

public class MemesanJasaServiceImpl implements MemesanJasaService{
	private EntityManagerFactory emf;
	private PenjualanJasaRepository jasarepo;
	private Map<Jasa , Integer> jasaa = new HashMap<>();
	private Map<PenjualanJasa, Integer> jasa2 = new HashMap<>();
	@Override
	public void addPenjualan(Jasa jasa) {
		if(jasaa.containsKey(jasa)) {
			jasaa.replace(jasa, jasaa.get(jasa)+1);
		}else {
			jasaa.put(jasa, 1);
		}
	}

	@Override
	public void removePenjualan(Jasa jasa) {
		if(jasaa.containsKey(jasa)) {
			if(jasaa.get(jasa)>1) {
				jasaa.replace(jasa, jasaa.get(jasa)-1);
			}else if(jasaa.get(jasa)==1) {
				jasaa.remove(jasa);
			}
		}
	}
	@Override
	public Map<Jasa, Integer> getProductsInCart() {
		return Collections.unmodifiableMap(jasaa);
	}

	@Override
	public Map<PenjualanJasa, Integer> getPenjualanJasa() {
		return Collections.unmodifiableMap(jasa2);
	}
	
	@Override
	public void checkout() {
		PenjualanJasa p;
		for (Map.Entry<PenjualanJasa, Integer> entry : jasa2.entrySet()) {
            // Refresh quantity for every product before checking
            p = jasarepo.findOne(entry.getKey().getIdPenjualanJasa());
        }
        jasarepo.save(jasa2.keySet());
        jasarepo.flush();
        jasaa.clear();
	}
	
	@Override
	public BigDecimal getTotal() {
		BigDecimal total = BigDecimal.ZERO;
		  for (Map.Entry<PenjualanJasa, Integer> entry : jasa2.entrySet()) {
	            total = total.add(entry.getKey().getHargaJasa().multiply(new BigDecimal(entry.getValue())));
	        }
	        return total;	
	}
	
	@Override
	public MemesanJasa save(MemesanJasa jasa) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		MemesanJasa saved1 = em.merge(jasa);
		em.getTransaction().commit();
		return saved1;
	}
}
