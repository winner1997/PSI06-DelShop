package com.PSI.Impl;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.PSI.model.User;
import com.PSI.service.LoginService;
import com.PSI.service.PasswordService;
import com.PSI.repository.UserRepository;
@Service
public class LoginServiceImpl implements LoginService{
	private UserRepository uRepo;
	private PasswordService passwords;
	private EntityManagerFactory en;
	
	public PasswordService getPasswords() {
		return passwords;
	}
	@Autowired
	public void setPasswords(PasswordService passwords) {
		this.passwords = passwords;
	}

	@Autowired
	public void setEn(EntityManagerFactory en) {
		this.en = en;
	}
	@Override
	public List<User> getAllAkun() {
		EntityManager em = en.createEntityManager();
		return em.createQuery("from User", User.class).getResultList();
	}
	@Override
	public User login(String email, String password) {
		List<User> userList = getAllAkun();
		for (int i=0 ; i<userList.size(); i++) {			
			if(userList.get(i).getEmail().equals(email)) {
				if(passwords.matches(password, userList.get(i).getPassword())) {
					return userList.get(i);
				}
				else {
					return null;
				}
			}
		}
		return null;
	}
}
