package com.PSI.Impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import com.PSI.model.PemesananMenunggu;
import com.PSI.model.User;
import com.PSI.service.PemesananMenungguService;
import com.PSI.repository.PemesananMenungguRepository;
public class PemesananMenungguServiceImpl implements PemesananMenungguService {
	private EntityManagerFactory en;

	@Autowired
	public void setEn(EntityManagerFactory en) {
		this.en = en;
	}
	@Autowired
	private PemesananMenungguRepository pmr;
	
	
	@Override
	public List<PemesananMenunggu> findall() {
		EntityManager em = en.createEntityManager();
		return em.createQuery("from PemesananMenunggu",PemesananMenunggu.class).getResultList();
	}

	@Override
	public PemesananMenunggu findByIdPembeli(int id) {
		List<PemesananMenunggu> ambil = new ArrayList<PemesananMenunggu>();
		List<PemesananMenunggu> pesan = findall();
		for(int i = 0; i < pesan.size(); i++ ) {
			if(pesan.get(i).getIdpembeli()==id) {
				ambil .add(pesan.get(i));
			}
		}
		return (PemesananMenunggu) ambil;
	}

	@Override
	public PemesananMenunggu findByIdPenjual(int id) {
		PemesananMenunggu ambil = (PemesananMenunggu) pmr.findAll();
		
		return ambil;
	}


}
