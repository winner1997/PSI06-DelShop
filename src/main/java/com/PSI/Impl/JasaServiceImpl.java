package com.PSI.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.PSI.model.Jasa;
import com.PSI.model.ViewPenjualanJasa;
import com.PSI.service.JasaService;
import com.PSI.repository.JasaRepository;
import com.PSI.repository.ViewPenjualanJasaRepository;

public class JasaServiceImpl implements JasaService {
	@Autowired
	private ViewPenjualanJasaRepository vp;
	private JasaRepository JR;

	List<ViewPenjualanJasa> list = null; 
	@Override
	public Jasa findJasaByIdjasa(int idJasa) {
		// TODO Auto-generated method stub
		return JR.findOne(idJasa);
	}

	@Override
	public Page<Jasa> findAllJasaPageable(Pageable pageable) {
		// TODO Auto-generated method stub
		return JR.findAll(pageable);
	}

	@Override
	public List<ViewPenjualanJasa> findAll() {
		return vp.findAll();
	}

	@Override
	public List<ViewPenjualanJasa> findByIdKategori(int id) {
		
		return findAll();
	}
	

}
