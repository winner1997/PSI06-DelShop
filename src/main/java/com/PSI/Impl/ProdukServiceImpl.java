package com.PSI.Impl;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.PSI.repository.ProdukRepository;
import com.PSI.model.Penjualan;
import com.PSI.model.Produk;
import com.PSI.service.ProdukService;

public class ProdukServiceImpl implements ProdukService{
	@Autowired
    private ProdukRepository produkRepository;
	@Autowired
	private Penjualan penjualan;
	@Autowired
	private EntityManagerFactory emf;
	public void setEmf(EntityManagerFactory emf) {
		this.emf = emf;
	}
	@Override
	public Page<Produk> findAllProductsPageable(Pageable pageable) {
	    return produkRepository.findAll(pageable);
	}
	@Override
	public Produk findByIdProduk(int id_produk) {
		return produkRepository.findOne(id_produk);
	}
	@Override
	public List<Produk> findAllProduk(){
		EntityManager em = emf.createEntityManager();
		return em.createQuery("from Produk", Produk.class).getResultList();
	}
	@Override
	public List<Produk> findByIdUser(int id) {
		List<Produk> produkList = findAllProduk();
		List<Produk> produkUser = null;
		for(int i=0;i<produkList.size();i++) {
			if(produkList.get(i).getUser().equals(id)) {
				produkUser.add(produkList.get(i));
			}
			else {
				continue;
			}
		}
		return produkUser;
	}
}
