package com.PSI.Impl;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.springframework.stereotype.Service;
import com.PSI.controller.Exception.NotEnoughProdukInStokException;
import com.PSI.model.MemesanJasa;
import com.PSI.model.MemesanProduk;
import com.PSI.model.PenjualanProduk;
import com.PSI.model.Produk;
import com.PSI.service.MemesanProdukService;
import com.PSI.repository.PenjualanProdukRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

@Service
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
@Transactional
public class MemesanProdukServiceImpl implements MemesanProdukService{
	private EntityManagerFactory emf;
	@Autowired
	private PenjualanProdukRepository produkRepository;
	
	private Map<Produk, Integer> Pproduk = new HashMap<>();
	private Map<PenjualanProduk, Integer> Pproduk2 = new HashMap<>();
	@Override
	public void addPenjualan(Produk produk) {
		// TODO Auto-generated method stub
		if(Pproduk.containsKey(produk)) {
			Pproduk.replace(produk, Pproduk.get(produk)+1);
		}
		else {
			Pproduk.put(produk, 1);
		}
	}
	
	@Override
	public void removePenjualan(Produk produk) {
		Pproduk.clear();
	}
	@Override
	public Map<Produk, Integer> getProductsInCart() {
		// TODO Auto-generated method stub		
		return Collections.unmodifiableMap(Pproduk);
	}
	
	@Override
	public void checkout() throws NotEnoughProdukInStokException {
		PenjualanProduk p; 
        for (Map.Entry<PenjualanProduk, Integer> entry : Pproduk2.entrySet()) {
            // Refresh quantity for every product before checking
            p = produkRepository.findByProduk(entry.getKey().getProduk());
            if (p.getStok() < entry.getValue())
                throw new NotEnoughProdukInStokException(p);
            entry.getKey().setStok(p.getStok() - entry.getValue());
        }
        produkRepository.save(Pproduk2.keySet());
        produkRepository.flush();
        Pproduk.clear();
	}
	@Override
	public BigDecimal getTotal() {
		BigDecimal total = BigDecimal.ZERO;
		  for (Map.Entry<PenjualanProduk,Integer> entry : Pproduk2.entrySet()) {
	            total = total.add(entry.getKey().getHargaProduk().multiply(new BigDecimal(entry.getValue())));
	        }
	        return total;
	}
	@Override
	public Map<PenjualanProduk, Integer> getPenjualanProduk() {
		return Collections.unmodifiableMap(Pproduk2);
	}
	@Override
	public MemesanProduk save (MemesanProduk produk) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		MemesanProduk saved = em.merge(produk);
		em.getTransaction().commit();
		return saved;
	}
}
