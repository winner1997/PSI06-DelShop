package com.PSI.Impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import com.PSI.model.Jasa;
import com.PSI.model.PenjualanJasa;
import com.PSI.repository.JasaRepository;
import com.PSI.repository.PenjualanJasaRepository;
import com.PSI.service.PenjualanJasaService;

public class PenjualanJasaServiceImpl implements PenjualanJasaService{
	private EntityManagerFactory emf;
	private PenjualanJasaRepository PJ;
	private JasaRepository J;
	@Override
	public Jasa tambahJasa(Jasa jasa) {
		EntityManager em = emf.createEntityManager(); //
		em.getTransaction().begin();
		Jasa saved = em.merge(jasa);
		em.getTransaction().commit();
		return saved;
	}
		
	@Override
	public PenjualanJasa tambahPenjualan(PenjualanJasa penjualanJasa) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		PenjualanJasa saved = em.merge(penjualanJasa);
		em.getTransaction().commit();
		return saved;
	}
	
	@Override
	public PenjualanJasa editPenjualan(PenjualanJasa edit) {
		// TODO Auto-generated method stub
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		PenjualanJasa saved = em.merge(edit);
		em.getTransaction().commit();
		return saved;
	}
	
	@Override
	public void hapusPenjualan(PenjualanJasa hapus) {
		PJ.delete(hapus);
	}
	@Override
	public void hapusJasa(Jasa hapus) {
		J.delete(hapus);
	}

	@Override
	public PenjualanJasa detailPenjualan(int id) {
		return PJ.getOne(id);
	}
	@Override
	public Jasa detailJasa(int id) {
		return J.getOne(id);
	}
	@Override
	public List<PenjualanJasa> getAllPenjualan() {
		// TODO Auto-generated method stub
		return PJ.findAll();
	}

	@Override
	public List<Jasa> getAllJasa() {
		// TODO Auto-generated method stub
		return J.findAll();
	}
}	