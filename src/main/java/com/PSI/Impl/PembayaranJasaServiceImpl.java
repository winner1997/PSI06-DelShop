package com.PSI.Impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import com.PSI.model.PembayaranJasa;
import com.PSI.model.User;
import com.PSI.service.PembayaranJasaService;
import com.PSI.repository.PembayaranJasaRepository;
public class PembayaranJasaServiceImpl implements PembayaranJasaService {
	private PembayaranJasaRepository PJ;
	private EntityManagerFactory emf;
	User useraktif;
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	Date date = new Date();
	@Override
	public PembayaranJasa pembayaranJasa(PembayaranJasa pembayaran) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		PembayaranJasa saved = em.merge(pembayaran);
		em.getTransaction().commit();
		return saved;
	}
	@Override
	public PembayaranJasa detailPembayaranJasa(int id) {
		// TODO Auto-generated method stub
		return PJ.getOne(id);
	}
	@Override
	public List<PembayaranJasa> getAllPembayaranJasa(){
		return PJ.findAll();
	}
	@Override
	public PembayaranJasa getPembayaranJasaUser() {
		List<PembayaranJasa> ambil = getAllPembayaranJasa();
		PembayaranJasa find = null;
		for (PembayaranJasa p : ambil) {
			find=p;
		}
		return find;
	}
}