package com.PSI.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "kategori_jasa")

public class KategoriJasa {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	
	@Column(name = "id_kategorijasa")
	private int idKategoriJasa;
	
	@Column(name = "nama_kategorijasa")
	private String namaKategorijasa;
	
	
	@OneToMany (mappedBy = "kategorijasa")
	private Set<DetailKategoriJasa> detailkategorijasa;
	
	
	public Set<DetailKategoriJasa> getDetailkategorijasa() {
		return detailkategorijasa;
	}
	public void setDetailkategorijasa(Set<DetailKategoriJasa> detailkategorijasa) {
		this.detailkategorijasa = detailkategorijasa;
	}
	public int getIdKategoriJasa() {
		return idKategoriJasa;
	}
	public void setIdKategoriJasa(int idKategoriJasa) {
		this.idKategoriJasa = idKategoriJasa;
	}
	public String getNamaKategorijasa() {
		return namaKategorijasa;
	}
	public void setNamaKategorijasa(String namaKategorijasa) {
		this.namaKategorijasa = namaKategorijasa;
	}
}
