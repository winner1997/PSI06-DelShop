package com.PSI.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;
@Entity
@Table(name = "pemesananmenunggu")
public class PemesananMenunggu {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)

	@Column(name = "id_produk")
	private int idproduk;
	
	@Column(name= "id_pemesanan")
	private int idpemesanan;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "idpenjual")
	private int idpenjual;
	
	@Column(name = "harga_produk")
	private BigDecimal hargaproduk;
	
	@Column(name = "total_harga")
	private BigDecimal totalharga;
	
	@Column(name = "jumlah")
	private int jumlah ;
	
	@Column(name = "idpembeli")
	private int idpembeli;
	
	@Column(name = "deskripsi")
	private String deskripsi;
	
	public String getDeskripsi() {
		return deskripsi;
	}
	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}

	public int getIdpemesanan() {
		return idpemesanan;
	}

	public void setIdpemesanan(int idpemesanan) {
		this.idpemesanan = idpemesanan;
	}

	public int getIdproduk() {
		return idproduk;
	}

	public void setIdproduk(int idproduk) {
		this.idproduk = idproduk;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getIdpenjual() {
		return idpenjual;
	}

	public void setIdpenjual(int idpenjual) {
		this.idpenjual = idpenjual;
	}

	public BigDecimal getHargaproduk() {
		return hargaproduk;
	}

	public void setHargaproduk(BigDecimal hargaproduk) {
		this.hargaproduk = hargaproduk;
	}

	public BigDecimal getTotalharga() {
		return totalharga;
	}

	public void setTotalharga(BigDecimal totalharga) {
		this.totalharga = totalharga;
	}

	public int getJumlah() {
		return jumlah;
	}

	public void setJumlah(int jumlah) {
		this.jumlah = jumlah;
	}

	public int getIdpembeli() {
		return idpembeli;
	}

	public void setIdpembeli(int idpembeli) {
		this.idpembeli = idpembeli;
	}
	
	
}
