package com.PSI.model;
	
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "pembayaran_jasa")
public class PembayaranJasa {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	
	@Column(name = "id_pembayaranjasa")
	private int idPembayaranJasa;
	
	@Column(name = "tanggal_pembayaran")
	private String tanggalPembayaran;
	
	@Column(name = "rincian_pembayaranjasa")
	private String rincianPembayaranJasa;
	
	@Column(name = "status_pembayaranjasa")
	private String statusPembayaranJasa;
	
	@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn (name = "id")
	private User user;
	
	@Column(name = "metode_pembayaran")
	private String metodePembayaran;
	
	@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn (name = "id_jasa")
	private Jasa jasa;
	
	@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn (name = "id_pemesananjasa")
	private MemesanJasa memesanjasa;
	
	public int getIdPembayaranJasa() {
		return idPembayaranJasa;
	}

	public void setIdPembayaranJasa(int idPembayaranJasa) {
		this.idPembayaranJasa = idPembayaranJasa;
	}

	public String getTanggalPembayaran() {
		return tanggalPembayaran;
	}

	public void setTanggalPembayaran(String tanggalPembayaran) {
		this.tanggalPembayaran = tanggalPembayaran;
	}

	public String getRincianPembayaranJasa() {
		return rincianPembayaranJasa;
	}

	public void setRincianPembayaranJasa(String rincianPembayaranJasa) {
		this.rincianPembayaranJasa = rincianPembayaranJasa;
	}

	public String getStatusPembayaranJasa() {
		return statusPembayaranJasa;
	}

	public void setStatusPembayaranJasa(String statusPembayaranJasa) {
		this.statusPembayaranJasa = statusPembayaranJasa;
	}

	

	public String getMetodePembayaran() {
		return metodePembayaran;
	}

	public void setMetodePembayaran(String metodePembayaran) {
		this.metodePembayaran = metodePembayaran;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Jasa getJasa() {
		return jasa;
	}

	public void setJasa(Jasa jasa) {
		this.jasa = jasa;
	}

	public MemesanJasa getMemesanjasa() {
		return memesanjasa;
	}

	public void setMemesanjasa(MemesanJasa memesanjasa) {
		this.memesanjasa = memesanjasa;
	}


}
