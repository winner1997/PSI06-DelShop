package com.PSI.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tinjauan_jasa")

public class TinjauanJasa {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	
	@Column(name = "id_tinjauanjasa")
	private int idTinjauanJasa;
	
	@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn (name = "id_jasa")
	private Jasa jasa;
	
	@Column(name = "komentar_jasa")
	private String komentarJasa;
	
	@Column(name = "tanggal_tinjauan")
	private String tanggalTinjauan;
	
	@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn (name = "id")
	private User user;

	public int getIdTinjauanJasa() {
		return idTinjauanJasa;
	}

	public void setIdTinjauanJasa(int idTinjauanJasa) {
		this.idTinjauanJasa = idTinjauanJasa;
	}


	public String getKomentarJasa() {
		return komentarJasa;
	}

	public void setKomentarJasa(String komentarJasa) {
		this.komentarJasa = komentarJasa;
	}

	public String getTanggalTinjauan() {
		return tanggalTinjauan;
	}

	public void setTanggalTinjauan(String tanggalTinjauan) {
		this.tanggalTinjauan = tanggalTinjauan;
	}

	public Jasa getJasa() {
		return jasa;
	}

	public void setJasa(Jasa jasa) {
		this.jasa = jasa;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
}
