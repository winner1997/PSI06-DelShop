package com.PSI.model;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "jasa")
public class Jasa {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	
	@Column(name = "id_jasa")
	private int idJasa;

	@OneToMany(mappedBy = "jasa")
	private Set<MemesanJasa> memesanjasa;
	
	@OneToMany(mappedBy = "jasa")
	private Set<PembayaranJasa> pembayaranjasa;
	
	@OneToMany(mappedBy = "jasa")
	private Set<PenjualanJasa> penjualanjasa;
	
	@OneToMany(mappedBy = "jasa")
	private Set<TinjauanJasa> tinjauanjasa;
	
	@Column( name = "deskripsi_jasa")
	private String deskripsiJasa;
	
	@Column (name = "id_kategorijasa")
	private int kategorijasa;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn (name = "id")
	private User user;
	
	@Column(name = "nama_jasa")
	private String nama_jasa;
	
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getNama_jasa() {
		return nama_jasa;
	}

	public void setNama_jasa(String nama_jasa) {
		this.nama_jasa = nama_jasa;
	}

	public Set<PembayaranJasa> getPembayaranjasa() {
		return pembayaranjasa;
	}

	public void setPembayaranjasa(Set<PembayaranJasa> pembayaranjasa) {
		this.pembayaranjasa = pembayaranjasa;
	}

	public Set<TinjauanJasa> getTinjauanjasa() {
		return tinjauanjasa;
	}

	public void setTinjauanjasa(Set<TinjauanJasa> tinjauanjasa) {
		this.tinjauanjasa = tinjauanjasa;
	}

	public int getIdJasa() {
		return idJasa;
	}

	public void setIdJasa(int idJasa) {
		this.idJasa = idJasa;
	}

	public String getDeskripsiJasa() {
		return deskripsiJasa;
	}

	public void setDeskripsiJasa(String deskripsiJasa) {
		this.deskripsiJasa = deskripsiJasa;
	}

	
	public int getKategorijasa() {
		return kategorijasa;
	}

	public void setKategorijasa(int kategori) {
		this.kategorijasa = kategori;
	}

	public Set<MemesanJasa> getMemesanjasa() {
		return memesanjasa;
	}

	public void setMemesanjasa(Set<MemesanJasa> memesanjasa) {
		this.memesanjasa = memesanjasa;
	}
	public Set<PenjualanJasa> getPenjualanjasa() {
		return penjualanjasa;
	}

	public void setPenjualanjasa(Set<PenjualanJasa> penjualanjasa) {
		this.penjualanjasa = penjualanjasa;
	}
}
