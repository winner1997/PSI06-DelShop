package com.PSI.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "penjualan")
public class Penjualan {
	@Id
	@GeneratedValue (strategy = GenerationType.AUTO)
	@Column(name= "id_produk")
	private int idProduk;
	
	@Column(name = "nama_produk")
	private String namaProduk;
	
	@Column(name="foto_produk")
	private String fotoProduk;
	
	@Column(name = "harga_produk")
	private int hargaProduk;
	
	@Column(name = "stok")
	private int stok;
	
	@Column(name = "tanggal_penjualanproduk")
	private String tanggalPenjualanProduk;
	
	public int getIdProduk() {
		return idProduk;
	}

	public void setIdProduk(int idProduk) {
		this.idProduk = idProduk;
	}

	public String getNamaProduk() {
		return namaProduk;
	}

	public void setNamaProduk(String namaProduk) {
		this.namaProduk = namaProduk;
	}

	public String getFotoProduk() {
		return fotoProduk;
	}

	public void setFotoProduk(String fotoProduk) {
		this.fotoProduk = fotoProduk;
	}

	public int getHargaProduk() {
		return hargaProduk;
	}

	public void setHargaProduk(int hargaProduk) {
		this.hargaProduk = hargaProduk;
	}

	public int getStok() {
		return stok;
	}

	public void setStok(int stok) {
		this.stok = stok;
	}

	public String getTanggalPenjualanProduk() {
		return tanggalPenjualanProduk;
	}

	public void setTanggalPenjualanProduk(String tanggalPenjualanProduk) {
		this.tanggalPenjualanProduk = tanggalPenjualanProduk;
	}
	
}
