package com.PSI.model;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "penjualan_jasa")

public class PenjualanJasa {
	@Id
	@GeneratedValue (strategy = GenerationType.AUTO)
	
	@Column(name = "id_penjualanjasa")
	private int idPenjualanJasa;
	
	@Column(name = "harga_jasa")
	private BigDecimal hargaJasa;
	
	@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn (name = "id_jasa")
	private Jasa jasa;
	
	@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn (name = "id")
	private User user;
	
	@Column(name = "status_jasa")
	private String statusJasa;
	
	public int getIdPenjualanJasa() {
		return idPenjualanJasa;
	}

	public void setIdPenjualanJasa(int idPenjualanJasa) {
		this.idPenjualanJasa = idPenjualanJasa;
	}

	public BigDecimal getHargaJasa() {
		return hargaJasa;
	}

	public void setHargaJasa(BigDecimal harga) {
		this.hargaJasa = harga;
	}
	
	public Jasa getJasa() {
		return jasa;
	}

	public void setJasa(Jasa jasa) {
		this.jasa = jasa;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getStatusJasa() {
		return statusJasa;
	}

	public void setStatusJasa(String statusJasa) {
		this.statusJasa = statusJasa;
	}

}
