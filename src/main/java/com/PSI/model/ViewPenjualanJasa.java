package com.PSI.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "penjualanjasa")
public class ViewPenjualanJasa {

	@Id
	@GeneratedValue (strategy = GenerationType.AUTO)
	@Column(name = "id_jasa")
	private int idjasa;
	
	@Column(name = "nama_jasa")
	private String namajasa;
	
	@Column(name = "id_kategorijasa")
	private int idkategorijasa;
	
	@Column(name = "harga_jasa")
	private BigDecimal hargajasa;
	
	@Column(name="status_jasa")
	private String statusjasa;

	public int getIdjasa() {
		return idjasa;
	}

	public void setIdjasa(int idjasa) {
		this.idjasa = idjasa;
	}

	public String getNamajasa() {
		return namajasa;
	}

	public void setNamajasa(String namajasa) {
		this.namajasa = namajasa;
	}

	public int getIdkategorijasa() {
		return idkategorijasa;
	}

	public void setIdkategorijasa(int idkategorijasa) {
		this.idkategorijasa = idkategorijasa;
	}

	public BigDecimal getHargajasa() {
		return hargajasa;
	}

	public void setHargajasa(BigDecimal hargajasa) {
		this.hargajasa = hargajasa;
	}

	public String getStatusjasa() {
		return statusjasa;
	}

	public void setStatusjasa(String statusjasa) {
		this.statusjasa = statusjasa;
	}
	
	
	
}
