package com.PSI.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "kategori_produk")

public class KategoriProduk {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_kategoriproduk")
	private int idKategoriProduk;
	
	@OneToMany (mappedBy = "kategoriproduk")
	private Set<Produk> produk;
 	
	@Column(name = "nama_kategoriproduk")
	private String namaKategoriProduk;
	
	
	
	public int getIdKategoriProduk() {
		return idKategoriProduk;
	}

	public void setIdKategoriProduk(int idKategoriProduk) {
		this.idKategoriProduk = idKategoriProduk;
	}
	public String getNamaKategoriProduk() {
		return namaKategoriProduk;
	}

	public void setNamaKategoriProduk(String namaKategoriProduk) {
		this.namaKategoriProduk = namaKategoriProduk;
	}

	public Set<Produk> getProduk() {
		return produk;
	}

	public void setProduk(Set<Produk> produk) {
		this.produk = produk;
	}	
	
}
