package com.PSI.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pembayaran")
public class Pembayaran {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_pemesananproduk")
	private int idPemesananProduk;
	
	@Column(name = "total_harga")
	private BigDecimal totalharga;
	
	@Column(name = "foto_produk")
	private String fotoproduk;
	
	@Column(name = "nama_produk")
	private String namaproduk;
	
	@Column(name = "jumlah")
	private int jumlah;
	@Column(name="deskripsi")
	private int deskripsi;
	
	public int getDeskripsi() {
		return deskripsi;
	}

	public void setDeskripsi(int deskripsi) {
		this.deskripsi = deskripsi;
	}

	public int getIdPemesananProduk() {
		return idPemesananProduk;
	}

	public void setIdPemesananProduk(int idPemesananProduk) {
		this.idPemesananProduk = idPemesananProduk;
	}

	public BigDecimal getTotalharga() {
		return totalharga;
	}

	public void setTotalharga(BigDecimal totalharga) {
		this.totalharga = totalharga;
	}

	public String getFotoproduk() {
		return fotoproduk;
	}

	public void setFotoproduk(String fotoproduk) {
		this.fotoproduk = fotoproduk;
	}

	public String getNamaproduk() {
		return namaproduk;
	}

	public void setNamaproduk(String namaproduk) {
		this.namaproduk = namaproduk;
	}

	public int getJumlah() {
		return jumlah;
	}

	public void setJumlah(int jumlah) {
		this.jumlah = jumlah;
	}
	
	
}
