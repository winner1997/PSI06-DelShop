package com.PSI.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tinjauan_produk")
public class TinjauanProduk {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	
	@Column(name = "id_tinjauanproduk")
	private int idTinjauanProduk;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_produk")
	private Produk produk;
	
	@Column(name = "komentar_produk")
	private String komentarProduk;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id")
	private User user;
	
	@Column(name = "tanggal_tinjauanproduk")
	private String tanggalTinjauanProduk;

	public int getIdTinjauanProduk() {
		return idTinjauanProduk;
	}

	public void setIdTinjauanProduk(int idTinjauanProduk) {
		this.idTinjauanProduk = idTinjauanProduk;
	}

	public Produk getProduk() {
		return produk;
	}

	public void setProduk(Produk produk) {
		this.produk = produk;
	}

	public String getKomentarProduk() {
		return komentarProduk;
	}

	public void setKomentarProduk(String komentarProduk) {
		this.komentarProduk = komentarProduk;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getTanggalTinjauanProduk() {
		return tanggalTinjauanProduk;
	}

	public void setTanggalTinjauanProduk(String tanggalTinjauanProduk) {
		this.tanggalTinjauanProduk = tanggalTinjauanProduk;
	}
	
	
}
