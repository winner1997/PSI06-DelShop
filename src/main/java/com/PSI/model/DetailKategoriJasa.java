package com.PSI.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "detail_kategorijasa")
public class DetailKategoriJasa {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)

	@Column(name = "id_detail")
	private int idDetail;

	@Column(name = "keterangan_detail")
	private String keteranganDetail;

	@Column(name = "nama_detail")
	private String namaDetail;

	@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn(name = "id_kategorijasa")
	private KategoriJasa kategorijasa;
	
	public int getIdDetail() {
		return idDetail;
	}

	public KategoriJasa getKategorijasa() {
		return kategorijasa;
	}


	public void setKategorijasa(KategoriJasa kategorijasa) {
		this.kategorijasa = kategorijasa;
	}


	public void setIdDetail(int idDetail) {
		this.idDetail = idDetail;
	}


	public String getKeteranganDetail() {
		return keteranganDetail;
	}

	public void setKeteranganDetail(String keteranganDetail) {
		this.keteranganDetail = keteranganDetail;
	}

	public String getNamaDetail() {
		return namaDetail;
	}

	public void setNamaDetail(String namaDetail) {
		this.namaDetail = namaDetail;
	}


}
