package com.PSI.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.PSI.model.Produk;

@Entity
@Table(name = "penjualan_produk")

public class PenjualanProduk {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	
	@Column(name = "id_penjualanproduk")
	private int idPenjualanProduk;
	
	@Column(name = "stok")
	private int stok;
	
	@Column(name = "harga_produk")
	private BigDecimal hargaProduk;
	
	@Column(name = "tanggal_penjualanproduk")
	private String tanggalPenjualanProduk;
	
	@Column(name = "status_produk")
	private String statusProduk;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id")
	private User user;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_produk")
	private Produk produk;
	
	public Produk getProduk() {
		return produk;
	}

	public void setProduk(Produk produk) {
		this.produk = produk;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getIdPenjualanProduk() {
		return idPenjualanProduk;
	}

	public void setIdPenjualanProduk(int idPenjualanProduk) {
		this.idPenjualanProduk = idPenjualanProduk;
	}

	public int getStok() {
		return stok;
	}

	public void setStok(int stok) {
		this.stok = stok;
	}

	public BigDecimal getHargaProduk() {
		return hargaProduk;
	}

	public void setHargaProduk(BigDecimal hargaProduk) {
		this.hargaProduk = hargaProduk;
	}

	public String getTanggalPenjualanProduk() {
		return tanggalPenjualanProduk;
	}

	public void setTanggalPenjualanProduk(String tanggalPenjualanProduk) {
		this.tanggalPenjualanProduk = tanggalPenjualanProduk;
	}

	public String getStatusProduk() {
		return statusProduk;
	}

	public void setStatusProduk(String statusProduk) {
		this.statusProduk = statusProduk;
	}

}

