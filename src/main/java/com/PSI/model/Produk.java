package com.PSI.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;



import javax.persistence.Entity;
import javax.persistence.FetchType;

@Entity
@Table(name = "produk")

public class Produk {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_produk")
	private int idProduk;
	
	@OneToMany (mappedBy = "produk")
	private Set<MemesanProduk> memesanproduk;
	
	@OneToMany (mappedBy = "produk")
	private Set<PembayaranProduk> pembayaranproduk;
	
	@OneToMany (mappedBy = "produk")
	private Set<PenjualanProduk> penjualanproduk;
	
	@OneToMany (mappedBy = "produk")
	private Set<TinjauanProduk> tinjauanproduk;
	
	
	@Column(name = "foto_produk")
	private String fotoProduk;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn (name = "id_kategoriproduk")
	private KategoriProduk kategoriproduk;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id")
	private User user;
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Column(name = "ciri_produk")
	private String ciriProduk;
	
	@Column(name = "deskripsi_produk")
	private String deskripsiProduk;
	
	@Column(name = "nama_produk")
	private String namaProduk;
	
	
	
	public Set<TinjauanProduk> getTinjauanproduk() {
		return tinjauanproduk;
	}

	public void setTinjauanproduk(Set<TinjauanProduk> tinjauanproduk) {
		this.tinjauanproduk = tinjauanproduk;
	}

	public Set<PenjualanProduk> getPenjualanproduk() {
		return penjualanproduk;
	}

	public void setPenjualanproduk(Set<PenjualanProduk> penjualanproduk) {
		this.penjualanproduk = penjualanproduk;
	}

	public Set<PembayaranProduk> getPembayaranproduk() {
		return pembayaranproduk;
	}

	public void setPembayaranproduk(Set<PembayaranProduk> pembayaranproduk) {
		this.pembayaranproduk = pembayaranproduk;
	}

	public KategoriProduk getKategoriproduk() {
		return kategoriproduk;
	}

	public void setKategoriproduk(KategoriProduk idkategori) {
		this.kategoriproduk = idkategori;
	}

	public Set<MemesanProduk> getMemesanproduk() {
		return memesanproduk;
	}

	public void setMemesanproduk(Set<MemesanProduk> memesanproduk) {
		this.memesanproduk = memesanproduk;
	}

	public int getIdProduk() {
		return idProduk;
	}

	public void setIdProduk(int idProduk) {
		this.idProduk = idProduk;
	}

	public String getFotoProduk() {
		return fotoProduk;
	}

	public void setFotoProduk(String string) {
		this.fotoProduk = string;
	}
	
	public String getCiriProduk() {
		return ciriProduk;
	}

	public void setCiriProduk(String ciriProduk) {
		this.ciriProduk = ciriProduk;
	}

	public String getDeskripsiProduk() {
		return deskripsiProduk;
	}

	public void setDeskripsiProduk(String deskripsiProduk) {
		this.deskripsiProduk = deskripsiProduk;
	}

	public String getNamaProduk() {
		return namaProduk;
	}

	public void setNamaProduk(String namaProduk) {
		this.namaProduk = namaProduk;
	}
}