package com.PSI.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Transient;

/**
 * @author Winner siringoringo
 *
 */
@Entity
@Table(name = "user")

public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)

	@Column(name = "id")
	private int id;
		
	@Column(name = "email", nullable = false, unique = true)
	@Email(message = "Please provide a valid e-mail")
	@NotEmpty(message = "Please provide an e-mail")
	private String email;
	
	@Column(name= "noTelp")
	private String noTelp;
	
	@Column(name = "password")
	@Transient
	private String password;
	@OneToMany(mappedBy = "user")
	private Set<MemesanProduk> memesanproduk;
	@OneToMany(mappedBy = "user")
	private Set<PembayaranProduk> pembayaranproduk;
	@OneToMany(mappedBy = "user")
	private Set<PenjualanProduk>penjualanproduk;
	@OneToMany(mappedBy = "user")
	private Set<TinjauanProduk>tinjauanproduk;
	
	@OneToMany(mappedBy = "user")
	private Set<Produk>produk;
	
	@OneToMany(mappedBy = "user")
	private Set<Jasa>jasa;
	
	@OneToMany(mappedBy = "user")
	private Set<PembayaranJasa>pembayaranjasa;
	@OneToMany(mappedBy = "user")
	private Set<PenjualanJasa> penjualanjasa;
	@OneToMany(mappedBy = "user")
	private Set<TinjauanJasa>tinjauanjasa;
	@OneToMany(mappedBy = "user")
	private Set<MemesanJasa> memesanjasa;
	
	public String getNoTelp() {
		return noTelp;
	}
	public void setNoTelp(String noTelp) {
		this.noTelp = noTelp;
	}
	public Set<Produk> getProduk() {
		return produk;
	}
	public void setProduk(Set<Produk> produk) {
		this.produk = produk;
	}
	public Set<TinjauanProduk> getTinjauanproduk() {
		return tinjauanproduk;
	}
	public void setTinjauanproduk(Set<TinjauanProduk> tinjauanproduk) {
		this.tinjauanproduk = tinjauanproduk;
	}
	public Set<PembayaranProduk> getPembayaranproduk() {
		return pembayaranproduk;
	}
	public void setPembayaranproduk(Set<PembayaranProduk> pembayaranproduk) {
		this.pembayaranproduk = pembayaranproduk;
	}
	public Set<MemesanProduk> getMemesanproduk() {
		return memesanproduk;
	}
	public void setMemesanproduk(Set<MemesanProduk> memesanproduk) {
		this.memesanproduk = memesanproduk;
	}
	public Set<PenjualanProduk> getPenjualanproduk() {
		return penjualanproduk;
	}
	public void setPenjualanproduk(Set<PenjualanProduk> penjualanproduk) {
		this.penjualanproduk = penjualanproduk;
	}
	@Column(name = "nama_depan")
	@NotEmpty(message = "masukkan nama depan anda")
	private String namaDepan;
	
	@Column(name = "nama_belakang")
	@NotEmpty(message = "masukkan nama belakang anda")
	private String namaBelakang;
	
	@Column(name = "enabled")
	private boolean enabled;
	
	@Column(name = "confirmation_token")
	private String confirmationToken;

	@Column(name = "foto_profil")
	private String fotoProfil;
	
	@Column(name = "bio")
	private String bio;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNamaDepan() {
		return namaDepan;
	}

	public void setNamaDepan(String namaDepan) {
		this.namaDepan = namaDepan;
	}

	public String getNamaBelakang() {
		return namaBelakang;
	}

	public void setNamaBelakang(String namaBelakang) {
		this.namaBelakang = namaBelakang;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getConfirmationToken() {
		return confirmationToken;
	}

	public void setConfirmationToken(String confirmationToken) {
		this.confirmationToken = confirmationToken;
	}

	public String getFotoProfil() {
		return fotoProfil;
	}

	public void setFotoProfil(String fotoProfil) {
		this.fotoProfil = fotoProfil;
	}

	public String getBio() {
		return bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}
	public Set<PembayaranJasa> getPembayaranjasa() {
		return pembayaranjasa;
	}
	public void setPembayaranjasa(Set<PembayaranJasa> pembayaranjasa) {
		this.pembayaranjasa = pembayaranjasa;
	}
	public Set<PenjualanJasa> getPenjualanjasa() {
		return penjualanjasa;
	}
	public void setPenjualanjasa(Set<PenjualanJasa> penjualanjasa) {
		this.penjualanjasa = penjualanjasa;
	}
	public Set<TinjauanJasa> getTinjauanjasa() {
		return tinjauanjasa;
	}
	public void setTinjauanjasa(Set<TinjauanJasa> tinjauanjasa) {
		this.tinjauanjasa = tinjauanjasa;
	}
	public Set<MemesanJasa> getMemesanjasa() {
		return memesanjasa;
	}
	public void setMemesanjasa(Set<MemesanJasa> memesanjasa) {
		this.memesanjasa = memesanjasa;
	}
}