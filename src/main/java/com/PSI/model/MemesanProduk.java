package com.PSI.model;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "memesan_produk")
public class MemesanProduk {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_pemesananproduk")
	private int idPemesananProduk;
	
	@OneToMany (mappedBy = "memesanproduk")
	private Set<PembayaranProduk>pembayaranproduk;
 	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn (name = "id_produk")
	private Produk produk;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn (name = "id")
	private User user;
	
	@Column(name = "total_harga")
	private int totalHarga;
	@Column(name = "ciri_produk")
	private String ciriProduk;
	
	@Column(name = "jumlah")
	private int jumlah;

	@Column(name = "status")
	private String status;
	
	public int getTotalHarga() {
		return totalHarga;
	}

	public void setTotalHarga(int i) {
		this.totalHarga = i;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Produk getProduk() {
		return produk;
	}

	public void setProduk(Produk id_produk) {
		this.produk = id_produk;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getIdPemesananProduk() {
		return idPemesananProduk;
	}

	public void setIdPemesananProduk(int idPemesananProduk) {
		this.idPemesananProduk = idPemesananProduk;
	}

	public String getCiriProduk() {
		return ciriProduk;
	}

	public void setCiriProduk(String ciriProduk) {
		this.ciriProduk = ciriProduk;
	}

	public int getJumlah() {
		return jumlah;
	}

	public void setJumlah(int jumlah) {
		this.jumlah = jumlah;
	}

	public Set<PembayaranProduk> getPembayaranproduk() {
		return pembayaranproduk;
	}

	public void setPembayaranproduk(Set<PembayaranProduk> pembayaranproduk) {
		this.pembayaranproduk = pembayaranproduk;
	}
	
}
