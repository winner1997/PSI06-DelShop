package com.PSI.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "pembayaran_produk")

public class PembayaranProduk {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	
	@Column(name = "id_pembayaranproduk")
	private int idPembayaranProduk;
	
	@Column(name = "tanggal_pembayaran")
	private String tanggalPembayaran;
	
	@Column(name = "metode_pembayaran")
	private String metodePembayaran;
	
	@Column(name = "detail_pembayaranproduk")
	private String detailPembayaranProduk;
	
	@Column(name = "status_pembayaran")
	private String status_pembayaran;
	
	
	@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn(name  = "id")
	private User user;
	
	@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn(name  = "id_produk")
	private Produk produk;


	@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn(name  = "id_pemesananproduk")
	private MemesanProduk memesanproduk;
	
	public MemesanProduk getMemesanproduk() {
		return memesanproduk;
	}

	public void setMemesanproduk(MemesanProduk memesanproduk) {
		this.memesanproduk = memesanproduk;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Produk getProduk() {
		return produk;
	}

	public void setProduk(Produk produk) {
		this.produk = produk;
	}

	public int getIdPembayaranProduk() {
		return idPembayaranProduk;
	}

	public void setIdPembayaranProduk(int idPembayaranProduk) {
		this.idPembayaranProduk = idPembayaranProduk;
	}

	public String getTanggalPembayaran() {
		return tanggalPembayaran;
	}

	public void setTanggalPembayaran(String tanggalPembayaran) {
		this.tanggalPembayaran = tanggalPembayaran;
	}

	public String getMetodePembayaran() {
		return metodePembayaran;
	}

	public void setMetodePembayaran(String metodePembayaran) {
		this.metodePembayaran = metodePembayaran;
	}

	public String getDetailPembayaranProduk() {
		return detailPembayaranProduk;
	}

	public void setDetailPembayaranProduk(String detailPembayaranProduk) {
		this.detailPembayaranProduk = detailPembayaranProduk;
	}

	public String getStatus_pembayaran() {
		return status_pembayaran;
	}

	public void setStatus_pembayaran(String status_pembayaran) {
		this.status_pembayaran = status_pembayaran;
	}

	
}
