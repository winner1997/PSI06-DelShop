package com.PSI.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "memesan_jasa")
public class MemesanJasa {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	
	@Column(name = "id_pemesananjasa")
	private int idPemesananJasa;
	
	@OneToMany (mappedBy = "memesanjasa")
	private Set<PembayaranJasa> pembayaranjasa;
	
	@Column(name = "tanggal_terimajasa")
	private String tanggalTerimajasa;
	
	@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn (name = "id_jasa")
	private Jasa jasa;

	@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn (name = "id")
	private User user;
	@Column(name = "catatan")
	private String catatan;
	
	@Column(name = "status_pemesananjasa")
	private String statusPemesananJasa;	
	
	@Column(name = "id_pemesan")
	private int idpemesan;
	
	
	public int getIdpemesan() {
		return idpemesan;
	}

	public void setIdpemesan(int idpemesan) {
		this.idpemesan = idpemesan;
	}

	public Set<PembayaranJasa> getPembayaranjasa() {
		return pembayaranjasa;
	}

	public void setPembayaranjasa(Set<PembayaranJasa> pembayaranjasa) {
		this.pembayaranjasa = pembayaranjasa;
	}

	public Jasa getJasa() {
		return jasa;
	}

	public void setJasa(Jasa id_jasa) {
		this.jasa = id_jasa;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	

	public int getIdPemesananJasa() {
		return idPemesananJasa;
	}

	public void setIdPemesananJasa(int idPemesananJasa) {
		this.idPemesananJasa = idPemesananJasa;
	}

	public String getTanggalTerimajasa() {
		return tanggalTerimajasa;
	}

	public void setTanggalTerimajasa(String tanggalTerimajasa) {
		this.tanggalTerimajasa = tanggalTerimajasa;
	}

	public String getCatatan() {
		return catatan;
	}

	public void setCatatan(String catatan) {
		this.catatan = catatan;
	}

	public String getStatusPemesananJasa() {
		return statusPemesananJasa;
	}

	public void setStatusPemesananJasa(String statusPemesananJasa) {
		this.statusPemesananJasa = statusPemesananJasa;
	}
}
