package com.PSI.service;
import java.util.List;
import org.springframework.stereotype.Repository;
import com.PSI.model.User;

@Repository
public interface LoginService {
	User login(String email, String password);
	List <User> getAllAkun();
}
