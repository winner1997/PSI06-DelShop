//Kelompok 06_PSI_2018
package com.PSI.service;
import java.util.List;
import org.springframework.stereotype.Service;
import com.PSI.model.Jasa;
import com.PSI.model.PenjualanJasa;
@Service
public interface PenjualanJasaService {
	public Jasa tambahJasa(Jasa jasa);
	public PenjualanJasa tambahPenjualan (PenjualanJasa penjualanJasa);
	public PenjualanJasa editPenjualan (PenjualanJasa edit);
	public void hapusPenjualan(PenjualanJasa hapus);
	public void hapusJasa(Jasa jasa);
	public PenjualanJasa detailPenjualan (int id);
	public Jasa detailJasa (int id);
	List <PenjualanJasa> getAllPenjualan();
	List <Jasa> getAllJasa();
}
