//Kelompok 06_PSI_2018
package com.PSI.service;
import com.PSI.model.Produk;

import java.util.List;

import org.springframework.stereotype.Service;

import com.PSI.model.Penjualan;
import com.PSI.model.PenjualanProduk;
@Service
public interface PenjualanProdukService {
	public void tambahProduk (Produk produk);
	public void tambahPenjualan (PenjualanProduk produk);
	public PenjualanProduk editPenjualan (PenjualanProduk penjualanProduk);
	void hapusPenjualan (PenjualanProduk penjualanProduk);
	PenjualanProduk detailPenjualan (int id);
	Produk detailProduk (int id);
	List <PenjualanProduk> getAllPenjualan();
	List <Penjualan> getAllProduk();
}
