//Kelompok 06_PSI_2018
package com.PSI.service;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.PSI.model.Produk;
@Service
public interface ProdukService {
	Produk findByIdProduk (int idProduk);
    Page<Produk> findAllProductsPageable(Pageable pageable);
    List<Produk>findAllProduk();
    List<Produk> findByIdUser(int id);
}
