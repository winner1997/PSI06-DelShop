package com.PSI.service;
import java.util.List;

import org.springframework.stereotype.Service;

import com.PSI.model.PemesananMenunggu;
@Service
public interface PemesananMenungguService {
	PemesananMenunggu findByIdPembeli(int id);
	PemesananMenunggu findByIdPenjual(int id);
	List<PemesananMenunggu> findall();
}
