package com.PSI.service;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.PSI.model.Penjualan;
@Service
public interface PenjualanService {
	Optional<Penjualan> findByIdProduk(int id);
    Page<Penjualan> findAllProductsPageable(Pageable pageable);
}
