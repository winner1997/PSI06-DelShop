//Kelompok 06_PSI_2018
package com.PSI.service;

import com.PSI.model.MemesanProduk;
import com.PSI.model.Penjualan;
import com.PSI.model.PenjualanProduk;
import com.PSI.model.Produk;
import com.PSI.controller.Exception.*;

import java.math.BigDecimal;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
@Service
public interface MemesanProdukService{
    void addPenjualan(Produk produk);
    void removePenjualan(Produk penjualanProduk);
    Map<Produk, Integer> getProductsInCart();
    Map<PenjualanProduk, Integer> getPenjualanProduk();
    void checkout() throws NotEnoughProdukInStokException;
    BigDecimal getTotal();
    MemesanProduk save(MemesanProduk produk);
}
