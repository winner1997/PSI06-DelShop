package com.PSI.service;
import java.math.BigDecimal;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.PSI.model.PenjualanJasa;
import com.PSI.model.Jasa;
import com.PSI.model.MemesanJasa;
@Service
public interface MemesanJasaService {
	void addPenjualan(Jasa jasa);
    void removePenjualan(Jasa jasa);
    Map<Jasa, Integer> getProductsInCart();
    Map<PenjualanJasa, Integer> getPenjualanJasa();
    void checkout();
    BigDecimal getTotal();
    MemesanJasa save(MemesanJasa jasa);
}
