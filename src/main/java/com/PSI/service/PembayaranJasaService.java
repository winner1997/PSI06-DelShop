package com.PSI.service;
import java.util.List;
import org.springframework.stereotype.Service;
import com.PSI.model.PembayaranJasa;
@Service
public interface PembayaranJasaService {
	PembayaranJasa pembayaranJasa (PembayaranJasa pembayaran);
	PembayaranJasa detailPembayaranJasa (int id);
	List <PembayaranJasa> getAllPembayaranJasa();
	PembayaranJasa getPembayaranJasaUser();
}
