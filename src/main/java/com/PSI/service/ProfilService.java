package com.PSI.service;

import org.springframework.stereotype.Service;
import com.PSI.model.User;
@Service
public interface ProfilService {
	User profil(int id);
	User editProfil(User profil);
}
