//Kelompok 06_PSI_2018
package com.PSI.service;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.PSI.model.Jasa;
import com.PSI.model.ViewPenjualanJasa;

@Service
public interface JasaService {
	Jasa findJasaByIdjasa (int idJasa);
    Page <Jasa> findAllJasaPageable(Pageable pageable);
    List<ViewPenjualanJasa> findAll();
    List<ViewPenjualanJasa> findByIdKategori(int id);
}
