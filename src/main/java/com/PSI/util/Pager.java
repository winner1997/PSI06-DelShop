package com.PSI.util;


import com.PSI.model.Penjualan;
import com.PSI.model.Produk;
import org.springframework.data.domain.Page;

public class Pager {

    private final Page<Penjualan> products;

    public Pager(Page<Penjualan> products2) {
        this.products = products2;
    }

    public int getPageIndex() {
        return products.getNumber() + 1;
    }

    public int getPageSize() {
        return products.getSize();
    }

    public boolean hasNext() {
        return products.hasNext();
    }

    public boolean hasPrevious() {
        return products.hasPrevious();
    }

    public int getTotalPages() {
        return products.getTotalPages();
    }

    public long getTotalElements() {
        return products.getTotalElements();
    }

    public boolean indexOutOfBounds() {
        return this.getPageIndex() < 0 || this.getPageIndex() > this.getTotalElements();
    }

}
