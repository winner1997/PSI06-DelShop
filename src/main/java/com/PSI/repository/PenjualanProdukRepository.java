//Kelompok 06_PSI_2018
package com.PSI.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.PSI.model.PenjualanProduk;
import com.PSI.model.Produk;
@Repository
public interface PenjualanProdukRepository extends JpaRepository <PenjualanProduk , Integer>{
	PenjualanProduk findByProduk(Produk produk);
}
