//Kelompok 06_PSI_2018
package com.PSI.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.PSI.model.PembayaranJasa;
import com.PSI.model.User;
@Repository
public interface PembayaranJasaRepository extends JpaRepository <PembayaranJasa , Integer>{
	PembayaranJasa findByIdPembayaranJasa (int idPembayaranJasa);
	List<PembayaranJasa> findByUser(User user);
}
