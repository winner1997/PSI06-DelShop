//Kelompok 06_PSI_2018
package com.PSI.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.PSI.model.KategoriProduk;
@Repository
public interface KategoriProdukRepository extends JpaRepository <KategoriProduk , String>{
	KategoriProduk findByIdKategoriProduk (int kategoriProduk);
}
