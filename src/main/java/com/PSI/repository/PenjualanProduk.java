package com.PSI.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.PSI.model.Penjualan;

public interface PenjualanProduk extends JpaRepository <Penjualan , String> {
	
}
