package com.PSI.repository;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;

import com.PSI.model.ViewPenjualanJasa;
import com.PSI.model.User;
public interface ViewPenjualanJasaRepository extends JpaRepository <ViewPenjualanJasa , Integer>{

	List<ViewPenjualanJasa> findByIdkategorijasa(int id); 
}
