package com.PSI.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.PSI.model.MemesanJasa;
import com.PSI.model.User;
@Repository
public interface MemesanJasaRepository extends JpaRepository <MemesanJasa , Integer>{
	@Modifying
	@Query("SELECT t from MemesanJasa t where t.statusPemesananJasa = ?1 and t.user = ?2")
	MemesanJasa findByStatusMenunggu(@Param("statusPemesananJasa") String statusPemesananJasa,@Param("user") User user);	
	MemesanJasa findByIdPemesananJasa (int idPemesananJasa);	
	
	List<MemesanJasa> findByidpemesan(int id);
}
