package com.PSI.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.PSI.model.PemesananMenunggu;

public interface PemesananMenungguUserRepository extends JpaRepository <PemesananMenunggu , String> {
	PemesananMenunggu findByidpenjual(int id); 
}
