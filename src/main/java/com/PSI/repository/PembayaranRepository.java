package com.PSI.repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.PSI.model.Pembayaran;
public interface PembayaranRepository extends JpaRepository<Pembayaran , Integer>{
	Pembayaran findByidPemesananProduk(int id);
}
