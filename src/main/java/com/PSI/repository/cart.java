package com.PSI.repository;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.PSI.model.Produk;

@Repository
public interface cart extends JpaRepository <Produk , Integer> {
	Optional<Produk> findByIdProduk (int idProduk);
}
