package com.PSI.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.PSI.model.Penjualan;
import com.PSI.model.Produk;
import com.PSI.model.User;
@Repository
public interface PenjualanRepository extends JpaRepository<Penjualan , Long> {
	Optional<Penjualan> findByIdProduk(int id);
	List<User> findByIdProduk(Produk prod);
}
