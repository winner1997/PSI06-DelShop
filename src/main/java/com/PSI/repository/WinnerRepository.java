package com.PSI.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.PSI.model.Penjualan;

public interface WinnerRepository extends JpaRepository <Penjualan , Integer>{
	
}
