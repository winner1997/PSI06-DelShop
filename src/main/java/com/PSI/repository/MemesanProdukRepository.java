package com.PSI.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.PSI.model.MemesanJasa;
import com.PSI.model.MemesanProduk;
import com.PSI.model.User;
@Repository
public interface MemesanProdukRepository extends JpaRepository <MemesanProduk, Integer>{
	@Modifying
	@Query("SELECT t from MemesanProduk  t where t.status = ?1 and id= ?2")
	MemesanJasa findByStatusMenunggu(@Param("status") String status,@Param("id") User id);	
	MemesanProduk findByIdPemesananProduk (int idPemesananProduk);
}
