//Kelompok 06_PSI_2018
package com.PSI.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.PSI.model.DetailKategoriJasa;
@Repository
public interface DetailKategoriJasaRepository extends JpaRepository <DetailKategoriJasa,String> {
	DetailKategoriJasa findByIdDetail (int idDetail);
	DetailKategoriJasa findByNamaDetail(String nama);
}
