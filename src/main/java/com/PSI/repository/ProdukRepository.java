//Kelompok 06_PSI_2018
package com.PSI.repository;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.PSI.model.Produk;
import com.PSI.model.User;
@Repository
public interface ProdukRepository extends JpaRepository<Produk, Integer> {
	@Modifying
	@Query("SELECT t from Produk t where t.kategoriproduk = ?1")
	Produk findByKategoriProduk(@Param("kategoriproduk") Integer kategoriproduk);
	
	@Modifying
	@Query("SELECT p from Produk p where p.namaProduk like '%?1%' ")
	Produk findByNamaProduk (@Param("namaProduk") String namaProduk);
	
	@Modifying
	@Query("SELECT p from Produk p where p.id = ?1 ")
	Produk findByIdUser (@Param("id") int id);
	
	List<Produk>findByUser(User user);
	List<Produk> findByIdProduk(int idProduk);
	
}
