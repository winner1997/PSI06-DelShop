//Kelompok 06_PSI_2018
package com.PSI.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.PSI.model.Jasa;
import com.PSI.model.TinjauanJasa;

@Repository
public interface TinjauanJasaRepository extends JpaRepository <TinjauanJasa, Integer> {
	TinjauanJasa findByIdTinjauanJasa (int tinjauanJasa);

	List<TinjauanJasa> findByJasa(Jasa prod);
}
