//Kelompok 06_PSI_2018
package com.PSI.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.PSI.model.Jasa;
import com.PSI.model.Produk;
import com.PSI.model.User;

@Repository
public interface JasaRepository extends JpaRepository<Jasa, Integer>{
	List<Jasa> findByIdJasa (int id_jasa);
	@Modifying
	@Query("SELECT j from Jasa j where j.nama_jasa like '%?1%' ")
	Jasa findByNamaJasa (@Param("nama_jasa") String nama_jasa);
	
	List<Jasa>findByUser(User user);
	
}	