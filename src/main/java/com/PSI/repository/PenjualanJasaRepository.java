//Kelompok 06_PSI_2018
package com.PSI.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.PSI.model.PenjualanJasa;

@Repository
public interface PenjualanJasaRepository extends JpaRepository <PenjualanJasa , Integer>{
	@Modifying
	@Query("SELECT j from PenjualanJasa j where j.jasa = ?1 ")
	PenjualanJasa findByIdPenjualanJasa (@Param("jasa") int jasa);
}
