//Kelompok 06_PSI_2018
package com.PSI.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.PSI.model.PembayaranProduk;
@Repository
public interface PembayaranProdukRepository extends JpaRepository <PembayaranProduk , Integer> {
	PembayaranProduk findByIdPembayaranProduk (int idPembayaranProduk);
}

