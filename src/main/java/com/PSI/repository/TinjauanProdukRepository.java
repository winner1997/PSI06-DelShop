//Kelompok 06_PSI_2018
package com.PSI.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.PSI.model.Produk;
import com.PSI.model.TinjauanProduk;

@Repository
public interface TinjauanProdukRepository extends JpaRepository <TinjauanProduk , String>{
	TinjauanProduk findByIdTinjauanProduk (int idTinjauanProduk);
	List<TinjauanProduk> findByProduk(Produk produk);
}
