//Kelompok 06_PSI_2018
package com.PSI.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.PSI.model.User;

@Repository("userRepository")
public interface UserRepository extends JpaRepository<User, Integer> {
	 User findByEmail(String email);
	 User findByConfirmationToken(String confirmationToken);
	 User findById(int id);
}