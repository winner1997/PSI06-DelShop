//Kelompok 06_PSI_2018
package com.PSI.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.PSI.model.KategoriJasa;
@Repository
public interface KategoriJasaRepository extends JpaRepository <KategoriJasa, String>{
	
	KategoriJasa findByNamaKategorijasa(String nama);
}

