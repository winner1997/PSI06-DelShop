package com.PSI.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.PSI.model.PemesananMenunggu;

public interface PemesananMenungguRepository extends JpaRepository <PemesananMenunggu,Integer> {
	List<PemesananMenunggu> findByidpembeli(int id);
	List<PemesananMenunggu> findByidpenjual(int id);

}	
