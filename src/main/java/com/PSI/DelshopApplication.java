//Kelompok 06_PSI_2018
package com.PSI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class DelshopApplication {

	public static void main(String[] args) {
		SpringApplication.run(DelshopApplication.class, args);
	}
}
